package charly.baquero.revoluttechnicaltest.screeens.exchangerates;

import android.os.Bundle;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.CurrencyExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.ExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter.CurrencyFormatterProvider;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.livedata.ExchangeRatesLiveData;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.livedata.RatesLiveData;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.GetExchangeRateDataSource;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.deserialaizers.ExchangeRatesResponseDeserializer;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.deserialaizers.RatesDeserializer;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.ExchangeRatesResponse;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.settings.RequestSettings;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.PublishSubject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

public final class ExchangeRatesPresenterTest {

    private static final String EUR_RATES_DATA = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6205,\"BGN\":1.9608,\"BRL\":4.804,\"CAD\":1.5377,\"CHF\":1.1304,\"CNY\":7.9654,\"CZK\":25.781,\"DKK\":7.4757,\"GBP\":0.90053,\"HKD\":9.1557,\"HRK\":7.4531,\"HUF\":327.32,\"IDR\":17368.0,\"ILS\":4.1812,\"INR\":83.931,\"ISK\":128.13,\"JPY\":129.88,\"KRW\":1308.1,\"MXN\":22.422,\"MYR\":4.8243,\"NOK\":9.8009,\"NZD\":1.7678,\"PHP\":62.752,\"PLN\":4.3293,\"RON\":4.6503,\"RUB\":79.778,\"SEK\":10.618,\"SGD\":1.6041,\"THB\":38.227,\"TRY\":7.6477,\"USD\":1.1664,\"ZAR\":17.869}}";

    private static final int NUMBER_OF_DECIMALS = 2;
    @NonNull
    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
    private static final boolean STRIP_TRAILING_ZEROS = true;

    @NonNull
    private final Gson gson = new GsonBuilder()
            .registerTypeAdapter(ExchangeRatesResponse.class, new ExchangeRatesResponseDeserializer())
            .registerTypeAdapter(new TypeToken<List<Rate>>() {
            }.getType(), new RatesDeserializer(new CurrencyExchangeCalculator("EUR", BigDecimal.ONE), new CurrencyFormatterProvider(NUMBER_OF_DECIMALS, ROUNDING_MODE, STRIP_TRAILING_ZEROS)))
            .create();

    @Mock
    private ExchangeRatesContract.View exchangeRateFragmentView;
    @Mock
    private GetExchangeRateDataSource getExchangeRateDataSource;
    @Mock
    private RequestSettings requestSettings;
    @Mock
    private ExchangeCalculator exchangeCalculator;
    @Mock
    private RatesLiveData ratesLiveData;

    private ExchangeRatesContract.Presenter exchangeRatePresenter;

    private TestScheduler testScheduler;

    @Rule
    public ExpectedException noExpectedException = ExpectedException.none();

    @Before
    public void setUp() {
        testScheduler = new TestScheduler();
        RxJavaPlugins.setComputationSchedulerHandler(scheduler -> testScheduler);
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() {
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
    }

    @Test
    public void requestExchangeRatesForCurrency_test() {
        // Given
        RatesLiveData ratesLiveData = new ExchangeRatesLiveData(exchangeRateFragmentView);
        exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);

        String currencyName = exchangeCalculator.getCurrencyNameToConvert();
        ExchangeRatesResponse exchangeRatesResponse = gson.fromJson(EUR_RATES_DATA, ExchangeRatesResponse.class);
        Mockito.when(requestSettings.getServerRequestInitialDelay()).thenReturn(0L);
        Mockito.when(requestSettings.getServerRequestPeriod()).thenReturn(1L);
        Mockito.when(requestSettings.getServerRequestTimeUnit()).thenReturn(TimeUnit.SECONDS);
        Mockito.when(getExchangeRateDataSource.getExchangeRateForCurrency(currencyName)).thenReturn(Single.just(exchangeRatesResponse));

        // When
        exchangeRatePresenter.requestExchangeRatesForCurrency();
        testScheduler.triggerActions();

        // Then
        Mockito.verify(requestSettings, times(1)).getServerRequestInitialDelay();
        Mockito.verify(requestSettings, times(1)).getServerRequestPeriod();
        Mockito.verify(requestSettings, times(1)).getServerRequestTimeUnit();
        Mockito.verify(exchangeRateFragmentView, times(1)).displayProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(1)).displayCurrenciesList();
        Mockito.verify(exchangeRateFragmentView, times(1)).hideErrorView();
        Mockito.verify(exchangeRateFragmentView, times(1)).hideProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(0)).hideCurrenciesList();
        Mockito.verify(exchangeRateFragmentView, times(0)).displayErrorView();
        Mockito.verify(exchangeRateFragmentView, times(1)).notifyDataSetChanged();
        Mockito.verify(exchangeRateFragmentView, never()).notifyItemRangeChanged(isA(Integer.class), isA(Integer.class));
        Mockito.verify(exchangeRateFragmentView, never()).notifyItemMoved(isA(Integer.class), isA(Integer.class), isA(String.class), isA(BigDecimal.class));
    }

    @Test
    public void requestExchangeRatesForCurrency_3times_test() {
        // Given
        RatesLiveData ratesLiveData = new ExchangeRatesLiveData(exchangeRateFragmentView);
        exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);

        String currencyName = exchangeCalculator.getCurrencyNameToConvert();
        ExchangeRatesResponse exchangeRatesResponse = gson.fromJson(EUR_RATES_DATA, ExchangeRatesResponse.class);
        Mockito.when(requestSettings.getServerRequestInitialDelay()).thenReturn(0L);
        Mockito.when(requestSettings.getServerRequestPeriod()).thenReturn(1L);
        Mockito.when(requestSettings.getServerRequestTimeUnit()).thenReturn(TimeUnit.SECONDS);
        Mockito.when(getExchangeRateDataSource.getExchangeRateForCurrency(currencyName)).thenReturn(Single.just(exchangeRatesResponse));

        // When
        exchangeRatePresenter.requestExchangeRatesForCurrency();
        testScheduler.triggerActions();
        exchangeRatePresenter.requestExchangeRatesForCurrency();
        testScheduler.triggerActions();
        exchangeRatePresenter.requestExchangeRatesForCurrency();
        testScheduler.triggerActions();

        // Then
        Mockito.verify(requestSettings, times(3)).getServerRequestInitialDelay();
        Mockito.verify(requestSettings, times(3)).getServerRequestPeriod();
        Mockito.verify(requestSettings, times(3)).getServerRequestTimeUnit();
        ArgumentCaptor<Integer> fromPositionArgumentCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> toPositionArgumentCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(exchangeRateFragmentView, times(3)).displayProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(3)).displayCurrenciesList();
        Mockito.verify(exchangeRateFragmentView, times(3)).hideErrorView();
        Mockito.verify(exchangeRateFragmentView, times(3)).hideProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(0)).hideCurrenciesList();
        Mockito.verify(exchangeRateFragmentView, times(0)).displayErrorView();
        Mockito.verify(exchangeRateFragmentView, times(1)).notifyDataSetChanged();
        Mockito.verify(exchangeRateFragmentView, times(1)).notifyDataSetChanged();
        Mockito.verify(exchangeRateFragmentView, times(2)).notifyItemRangeChanged(fromPositionArgumentCaptor.capture(), toPositionArgumentCaptor.capture());
        Mockito.verify(exchangeRateFragmentView, never()).notifyItemMoved(isA(Integer.class), isA(Integer.class), isA(String.class), isA(BigDecimal.class));
        assertEquals(1L, (long) fromPositionArgumentCaptor.getValue());
        assertEquals(33, (long) toPositionArgumentCaptor.getValue());
    }

    @Test
    public void onRateClick_NoPosition_test() {
        // Given
        RatesLiveData ratesLiveData = new ExchangeRatesLiveData(exchangeRateFragmentView);
        int positionClicked = RecyclerView.NO_POSITION;
        exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);

        // When
        exchangeRatePresenter.onRateClick(positionClicked);

        // Then
        Mockito.verifyNoInteractions(exchangeRateFragmentView);
    }

    @Test
    public void onRateClick_position3_test() {
        // Given
        String currencyName = "EUR";
        RatesLiveData ratesLiveData = new ExchangeRatesLiveData(exchangeRateFragmentView);
        Mockito.when(exchangeCalculator.getCurrencyNameToConvert()).thenReturn(currencyName);
        ExchangeRatesResponse exchangeRatesResponse = gson.fromJson(EUR_RATES_DATA, ExchangeRatesResponse.class);
        Mockito.when(requestSettings.getServerRequestInitialDelay()).thenReturn(0L);
        Mockito.when(requestSettings.getServerRequestPeriod()).thenReturn(1L);
        Mockito.when(requestSettings.getServerRequestTimeUnit()).thenReturn(TimeUnit.SECONDS);
        Mockito.when(getExchangeRateDataSource.getExchangeRateForCurrency(currencyName)).thenReturn(Single.just(exchangeRatesResponse));
        exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);
        exchangeRatePresenter.requestExchangeRatesForCurrency();
        testScheduler.triggerActions();

        int positionClicked = 3;

        // When
        exchangeRatePresenter.onRateClick(positionClicked);

        // Then
        Mockito.verify(requestSettings, times(1)).getServerRequestInitialDelay();
        Mockito.verify(requestSettings, times(1)).getServerRequestPeriod();
        Mockito.verify(requestSettings, times(1)).getServerRequestTimeUnit();
        Mockito.verify(exchangeRateFragmentView, times(1)).displayProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(1)).displayCurrenciesList();
        Mockito.verify(exchangeRateFragmentView, times(1)).hideErrorView();
        Mockito.verify(exchangeRateFragmentView, times(1)).hideProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(0)).hideCurrenciesList();
        Mockito.verify(exchangeRateFragmentView, times(0)).displayErrorView();
        Mockito.verify(exchangeRateFragmentView, times(1)).hideProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(1)).notifyDataSetChanged();
        Mockito.verify(exchangeRateFragmentView, never()).notifyItemRangeChanged(isA(Integer.class), isA(Integer.class));

        ArgumentCaptor<Integer> fromPositionArgumentCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> toPositionArgumentCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<String> currencyNameToConvertArgumentCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<BigDecimal> amountToConvertArgumentCaptor = ArgumentCaptor.forClass(BigDecimal.class);
        Mockito.verify(exchangeRateFragmentView, times(1)).notifyItemMoved(fromPositionArgumentCaptor.capture(), toPositionArgumentCaptor.capture(), currencyNameToConvertArgumentCaptor.capture(), amountToConvertArgumentCaptor.capture());
        assertEquals(3L, (long) fromPositionArgumentCaptor.getValue());
        assertEquals(0L, (long) toPositionArgumentCaptor.getValue());
        assertEquals("BRL", currencyNameToConvertArgumentCaptor.getValue());
        assertEquals(0, new BigDecimal("4.804").compareTo(amountToConvertArgumentCaptor.getValue()));
    }

    @Test
    public void updateDataToConvert_test() {
        // Given
        String currencyName = "EUR";
        ExchangeRatesResponse exchangeRatesResponse = gson.fromJson(EUR_RATES_DATA, ExchangeRatesResponse.class);
        Mockito.when(exchangeCalculator.getCurrencyNameToConvert()).thenReturn(currencyName);
        Mockito.when(requestSettings.getServerRequestInitialDelay()).thenReturn(0L);
        Mockito.when(requestSettings.getServerRequestPeriod()).thenReturn(1L);
        Mockito.when(requestSettings.getServerRequestTimeUnit()).thenReturn(TimeUnit.SECONDS);
        Mockito.when(getExchangeRateDataSource.getExchangeRateForCurrency(currencyName)).thenReturn(Single.just(exchangeRatesResponse));
        exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);

        String currencyNameToConvert = "EUR";
        BigDecimal amountToConvert = BigDecimal.TEN;

        // When
        exchangeRatePresenter.updateDataToConvert(currencyNameToConvert, amountToConvert);
        testScheduler.triggerActions();

        // Then
        Mockito.verify(exchangeCalculator, times(1)).setDataToConvert("EUR", BigDecimal.TEN);
        Mockito.verify(requestSettings, times(1)).getServerRequestInitialDelay();
        Mockito.verify(requestSettings, times(1)).getServerRequestPeriod();
        Mockito.verify(requestSettings, times(1)).getServerRequestTimeUnit();
        Mockito.verify(ratesLiveData, times(1)).setRatesList(anyList());
    }

    @Test
    public void getRateForPosition_test() {
        // Given
        String currencyName = "EUR";
        RatesLiveData ratesLiveData = new ExchangeRatesLiveData(exchangeRateFragmentView);

        ExchangeRatesResponse exchangeRatesResponse = gson.fromJson(EUR_RATES_DATA, ExchangeRatesResponse.class);
        Mockito.when(requestSettings.getServerRequestInitialDelay()).thenReturn(0L);
        Mockito.when(requestSettings.getServerRequestPeriod()).thenReturn(1L);
        Mockito.when(requestSettings.getServerRequestTimeUnit()).thenReturn(TimeUnit.SECONDS);
        Mockito.when(getExchangeRateDataSource.getExchangeRateForCurrency(currencyName)).thenReturn(Single.just(exchangeRatesResponse));
        Mockito.when(exchangeCalculator.getCurrencyNameToConvert()).thenReturn(currencyName);
        exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);
        exchangeRatePresenter.requestExchangeRatesForCurrency();
        testScheduler.triggerActions();

        // When
        Rate rate5 = exchangeRatePresenter.getRateForPosition(5);

        // Then
        Mockito.verify(exchangeRateFragmentView, times(1)).displayProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(1)).displayCurrenciesList();
        Mockito.verify(exchangeRateFragmentView, times(1)).hideErrorView();
        Mockito.verify(exchangeRateFragmentView, times(1)).hideProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(0)).hideCurrenciesList();
        Mockito.verify(exchangeRateFragmentView, times(0)).displayErrorView();
        Mockito.verify(requestSettings, times(1)).getServerRequestInitialDelay();
        Mockito.verify(requestSettings, times(1)).getServerRequestPeriod();
        Mockito.verify(requestSettings, times(1)).getServerRequestTimeUnit();

        assertNotNull(rate5);
        assertEquals("CHF", rate5.getName());
        assertEquals(0, new BigDecimal("1.1304").compareTo(rate5.getRate()));
        assertEquals(0, new BigDecimal("1.1304").compareTo(rate5.getConvertedAmount()));
        assertEquals("1.13", rate5.getFormattedConvertedAmount());
    }

    @Test
    public void getRatesItemCount_test() {
        // Given
        String currencyName = "EUR";
        RatesLiveData ratesLiveData = new ExchangeRatesLiveData(exchangeRateFragmentView);

        Mockito.when(exchangeCalculator.getCurrencyNameToConvert()).thenReturn(currencyName);
        ExchangeRatesResponse exchangeRatesResponse = gson.fromJson(EUR_RATES_DATA, ExchangeRatesResponse.class);
        Mockito.when(requestSettings.getServerRequestInitialDelay()).thenReturn(0L);
        Mockito.when(requestSettings.getServerRequestPeriod()).thenReturn(1L);
        Mockito.when(requestSettings.getServerRequestTimeUnit()).thenReturn(TimeUnit.SECONDS);
        Mockito.when(getExchangeRateDataSource.getExchangeRateForCurrency(currencyName)).thenReturn(Single.just(exchangeRatesResponse));
        exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);
        exchangeRatePresenter.requestExchangeRatesForCurrency();
        testScheduler.triggerActions();

        // When
        int ratesItemCount = exchangeRatePresenter.getRatesItemCount();

        // Then
        Mockito.verify(exchangeRateFragmentView, times(1)).displayProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(1)).displayCurrenciesList();
        Mockito.verify(exchangeRateFragmentView, times(1)).hideErrorView();
        Mockito.verify(exchangeRateFragmentView, times(1)).hideProgressBar();
        Mockito.verify(exchangeRateFragmentView, times(0)).hideCurrenciesList();
        Mockito.verify(exchangeRateFragmentView, times(0)).displayErrorView();
        Mockito.verify(requestSettings, times(1)).getServerRequestInitialDelay();
        Mockito.verify(requestSettings, times(1)).getServerRequestPeriod();
        Mockito.verify(requestSettings, times(1)).getServerRequestTimeUnit();
        Mockito.verify(exchangeRateFragmentView, times(1)).notifyDataSetChanged();
        Mockito.verify(exchangeRateFragmentView, never()).notifyItemRangeChanged(isA(Integer.class), isA(Integer.class));
        Mockito.verify(exchangeRateFragmentView, never()).notifyItemMoved(isA(Integer.class), isA(Integer.class), isA(String.class), isA(BigDecimal.class));

        assertEquals(33L, ratesItemCount);
    }

    @Test
    public void onAmountToConvertObservable_test() {
        // Given
        long debounceTime = 500L;
        TimeUnit debounceTimeUnit = TimeUnit.MILLISECONDS;
        PublishSubject<String> publishSubject = PublishSubject.create();
        Mockito.when(requestSettings.getDebounceTime()).thenReturn(debounceTime);
        Mockito.when(requestSettings.getDebounceTimeUnit()).thenReturn(debounceTimeUnit);
        exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);

        // When
        exchangeRatePresenter.onAmountToConvertObservable(publishSubject);
        testScheduler.triggerActions();
        publishSubject.onNext("1");
        testScheduler.advanceTimeBy(debounceTime, debounceTimeUnit);
        publishSubject.onNext("12");
        testScheduler.advanceTimeBy(debounceTime, debounceTimeUnit);
        publishSubject.onNext("12.");
        testScheduler.advanceTimeBy(debounceTime, debounceTimeUnit);
        publishSubject.onNext("12.6");
        testScheduler.advanceTimeBy(debounceTime, debounceTimeUnit);
        publishSubject.onNext("12.");
        testScheduler.advanceTimeBy(debounceTime, debounceTimeUnit);
        publishSubject.onNext("12");
        testScheduler.advanceTimeBy(debounceTime, debounceTimeUnit);
        publishSubject.onNext("1");
        testScheduler.advanceTimeBy(debounceTime, debounceTimeUnit);
        publishSubject.onNext("");
        testScheduler.advanceTimeBy(debounceTime, debounceTimeUnit);

        // Then
        Mockito.verify(exchangeCalculator, times(2)).setAmountToConvert(new BigDecimal("1"));
        Mockito.verify(exchangeCalculator, times(2)).setAmountToConvert(new BigDecimal("12"));
        Mockito.verify(exchangeCalculator, times(1)).setAmountToConvert(new BigDecimal("12.6"));
        Mockito.verify(ratesLiveData, times(5)).amountToConvertChanged();
    }

    @Test
    public void onPause_test() {
        // Given
        exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);

        // When
        exchangeRatePresenter.onPause();

        // Then
        Mockito.verify(exchangeRateFragmentView, times(1)).hideProgressBar();
        Mockito.verify(ratesLiveData, times(1)).clearData();
    }

    @Test
    public void onCreate_with_Null_savedInstanceState_test() {
        // Given
        Bundle outState = mockBundle();
        ExchangeCalculator exchangeCalculator = new CurrencyExchangeCalculator("GBP", new BigDecimal("27.77"));
        ExchangeRatesPresenter exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);

        // When
        exchangeRatePresenter.onSaveInstanceState(outState);

        // Then
        assertSame(exchangeCalculator, exchangeRatePresenter.getExchangeCalculator());
    }

    @Test
    public void onCreate_with_NonNull_savedInstanceState_test() {
        // Given
        Bundle outState = mockBundle();
        ExchangeCalculator exchangeCalculator = new CurrencyExchangeCalculator("GBP", new BigDecimal("27.77"));
        ExchangeRatesPresenter exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);
        exchangeRatePresenter.onSaveInstanceState(outState);

        // When
        exchangeRatePresenter.onCreate(outState);

        // Then
        assertSame(exchangeCalculator, exchangeRatePresenter.getExchangeCalculator());
    }

    @Test
    public void onSaveInstanceState_test() {
        // Given
        Bundle outState = mockBundle();
        ExchangeCalculator exchangeCalculator = new CurrencyExchangeCalculator("GBP", new BigDecimal("27.77"));
        exchangeRatePresenter = new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);

        // When
        exchangeRatePresenter.onSaveInstanceState(outState);

        // Then
        ExchangeCalculator exchangeCalculatorRetrieved = outState.getParcelable("ExchangeRatesPresenter.EXCHANGE_CALCULATOR_KEY");
        assertNotNull(exchangeCalculatorRetrieved);
        assertEquals(0, exchangeCalculator.getAmountToConvert().compareTo(exchangeCalculatorRetrieved.getAmountToConvert()));
        assertEquals(exchangeCalculator.getCurrencyNameToConvert(), exchangeCalculatorRetrieved.getCurrencyNameToConvert());
    }

    @NonNull
    private Bundle mockBundle() {
        final Map<String, Parcelable> fakeBundle = new HashMap<>();
        Bundle bundle = mock(Bundle.class);

        doAnswer(invocation -> {
            Object[] arguments = invocation.getArguments();
            String key = (String) arguments[0];
            Parcelable value = (Parcelable) arguments[1];
            fakeBundle.put(key, value);
            return null;
        }).when(bundle).putParcelable(anyString(), any(Parcelable.class));

        doAnswer(invocation -> {
            Object[] arguments = invocation.getArguments();
            String key = (String) arguments[0];
            return fakeBundle.get(key);
        }).when(bundle).getParcelable(anyString());

        return bundle;
    }
}