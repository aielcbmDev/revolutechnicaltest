package charly.baquero.revoluttechnicaltest.screeens.exchangerates;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.CurrencyExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.ExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter.CurrencyFormatter;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter.CurrencyFormatterProvider;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.livedata.ExchangeRatesLiveData;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.livedata.RatesLiveData;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.deserialaizers.ExchangeRatesResponseDeserializer;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.deserialaizers.RatesDeserializer;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.ExchangeRatesResponse;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

public final class ExchangeRatesLiveDataTest {

    private static final int NUMBER_OF_THREADS = 30;

    private static final String EUR_RATES_DATA = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6205,\"BGN\":1.9608,\"BRL\":4.804,\"CAD\":1.5377,\"CHF\":1.1304,\"CNY\":7.9654,\"CZK\":25.781,\"DKK\":7.4757,\"GBP\":0.90053,\"HKD\":9.1557,\"HRK\":7.4531,\"HUF\":327.32,\"IDR\":17368.0,\"ILS\":4.1812,\"INR\":83.931,\"ISK\":128.13,\"JPY\":129.88,\"KRW\":1308.1,\"MXN\":22.422,\"MYR\":4.8243,\"NOK\":9.8009,\"NZD\":1.7678,\"PHP\":62.752,\"PLN\":4.3293,\"RON\":4.6503,\"RUB\":79.778,\"SEK\":10.618,\"SGD\":1.6041,\"THB\":38.227,\"TRY\":7.6477,\"USD\":1.1664,\"ZAR\":17.869}}";
    private static final String BGN_RATES_DATA = "{\"base\":\"BGN\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.82931,\"BRL\":2.4585,\"CAD\":0.78692,\"CHF\":0.57847,\"CNY\":4.0762,\"CZK\":13.193,\"DKK\":3.8257,\"GBP\":0.46084,\"HKD\":4.6854,\"HRK\":3.8141,\"HUF\":167.51,\"IDR\":8887.7,\"ILS\":2.1397,\"INR\":42.952,\"ISK\":65.568,\"JPY\":66.467,\"KRW\":669.43,\"MXN\":11.475,\"MYR\":2.4688,\"NOK\":5.0156,\"NZD\":0.90467,\"PHP\":32.113,\"PLN\":2.2155,\"RON\":2.3798,\"RUB\":40.826,\"SEK\":5.4335,\"SGD\":0.82087,\"THB\":19.563,\"TRY\":3.9137,\"USD\":0.59688,\"ZAR\":9.1444,\"EUR\":0.51305}}";
    private static final String AUD_RATES_DATA = "{\"base\":\"AUD\",\"date\":\"2018-09-06\",\"rates\":{\"BGN\":1.2112,\"BRL\":2.9676,\"CAD\":0.94993,\"CHF\":0.69826,\"CNY\":4.9205,\"CZK\":15.926,\"DKK\":4.618,\"GBP\":0.5563,\"HKD\":5.6558,\"HRK\":4.6041,\"HUF\":202.2,\"IDR\":10729.0,\"ILS\":2.5829,\"INR\":51.848,\"ISK\":79.149,\"JPY\":80.232,\"KRW\":808.07,\"MXN\":13.851,\"MYR\":2.9801,\"NOK\":6.0544,\"NZD\":1.092,\"PHP\":38.764,\"PLN\":2.6744,\"RON\":2.8727,\"RUB\":49.282,\"SEK\":6.5589,\"SGD\":0.99088,\"THB\":23.615,\"TRY\":4.7243,\"USD\":0.72053,\"ZAR\":11.038,\"EUR\":0.61932}}";
    private static final String CAD_RATES_DATA = "{\"base\":\"CAD\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.0519,\"BGN\":1.2727,\"BRL\":3.1182,\"CHF\":0.73369,\"CNY\":5.1702,\"CZK\":16.734,\"DKK\":4.8524,\"GBP\":0.58452,\"HKD\":5.9428,\"HRK\":4.8377,\"HUF\":212.46,\"IDR\":11273.0,\"ILS\":2.714,\"INR\":54.478,\"ISK\":83.162,\"JPY\":84.303,\"KRW\":849.03,\"MXN\":14.554,\"MYR\":3.1313,\"NOK\":6.3617,\"NZD\":1.1474,\"PHP\":40.732,\"PLN\":2.8101,\"RON\":3.0185,\"RUB\":51.783,\"SEK\":6.8916,\"SGD\":1.0412,\"THB\":24.813,\"TRY\":4.964,\"USD\":0.7571,\"ZAR\":11.598,\"EUR\":0.65074}}";
    private static final String DKK_RATES_DATA = "{\"base\":\"DKK\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.21638,\"BGN\":0.26182,\"BRL\":0.64147,\"CAD\":0.20533,\"CHF\":0.15094,\"CNY\":1.0636,\"CZK\":3.4424,\"GBP\":0.12025,\"HKD\":1.2225,\"HRK\":0.99519,\"HUF\":43.707,\"IDR\":2319.1,\"ILS\":0.55831,\"INR\":11.207,\"ISK\":17.108,\"JPY\":17.343,\"KRW\":174.66,\"MXN\":2.9941,\"MYR\":0.64418,\"NOK\":1.3087,\"NZD\":0.23606,\"PHP\":8.3791,\"PLN\":0.57808,\"RON\":0.62096,\"RUB\":10.653,\"SEK\":1.4178,\"SGD\":0.2142,\"THB\":5.1044,\"TRY\":1.0212,\"USD\":0.15574,\"ZAR\":2.386,\"EUR\":0.13387}}";
    private static final String CZK_RATES_DATA = "{\"base\":\"CZK\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.063146,\"BGN\":0.076406,\"BRL\":0.1872,\"CAD\":0.059918,\"CHF\":0.044048,\"CNY\":0.31038,\"DKK\":0.2913,\"GBP\":0.035091,\"HKD\":0.35677,\"HRK\":0.29042,\"HUF\":12.755,\"IDR\":676.76,\"ILS\":0.16293,\"INR\":3.2705,\"ISK\":4.9928,\"JPY\":5.0608,\"KRW\":50.97,\"MXN\":0.87373,\"MYR\":0.18799,\"NOK\":0.38191,\"NZD\":0.068886,\"PHP\":2.4452,\"PLN\":0.1687,\"RON\":0.18121,\"RUB\":3.1086,\"SEK\":0.41373,\"SGD\":0.062504,\"THB\":1.4896,\"TRY\":0.298,\"USD\":0.045448,\"ZAR\":0.69628,\"EUR\":0.039066}}";
    private static final String MYR_RATES_DATA = "{\"base\":\"MYR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6205,\"BGN\":1.9608,\"BRL\":4.804,\"CAD\":1.5377,\"CHF\":1.1304,\"CNY\":7.9654,\"CZK\":25.781,\"DKK\":7.4757,\"GBP\":0.90053,\"HKD\":9.1557,\"HRK\":7.4531,\"HUF\":327.32,\"IDR\":17368.0,\"ILS\":4.1812,\"INR\":83.931,\"ISK\":128.13,\"JPY\":129.88,\"KRW\":1308.1,\"MXN\":22.422,\"MYR\":4.8243,\"NOK\":9.8009,\"NZD\":1.7678,\"PHP\":62.752,\"PLN\":4.3293,\"RON\":4.6503,\"RUB\":79.778,\"SEK\":10.618,\"SGD\":1.6041,\"THB\":38.227,\"TRY\":7.6477,\"USD\":1.1664,\"ZAR\":17.869}}";
    private static final String RON_RATES_DATA = "{\"base\":\"RON\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.82931,\"BRL\":2.4585,\"CAD\":0.78692,\"CHF\":0.57847,\"CNY\":4.0762,\"CZK\":13.193,\"DKK\":3.8257,\"GBP\":0.46084,\"HKD\":4.6854,\"HRK\":3.8141,\"HUF\":167.51,\"IDR\":8887.7,\"ILS\":2.1397,\"INR\":42.952,\"ISK\":65.568,\"JPY\":66.467,\"KRW\":669.43,\"MXN\":11.475,\"MYR\":2.4688,\"NOK\":5.0156,\"NZD\":0.90467,\"PHP\":32.113,\"PLN\":2.2155,\"RON\":2.3798,\"RUB\":40.826,\"SEK\":5.4335,\"SGD\":0.82087,\"THB\":19.563,\"TRY\":3.9137,\"USD\":0.59688,\"ZAR\":9.1444,\"EUR\":0.51305}}";
    private static final String PHP_RATES_DATA = "{\"base\":\"AUD\",\"date\":\"2018-09-06\",\"rates\":{\"BGN\":1.2112,\"BRL\":2.9676,\"CAD\":0.94993,\"CHF\":0.69826,\"CNY\":4.9205,\"CZK\":15.926,\"DKK\":4.618,\"GBP\":0.5563,\"HKD\":5.6558,\"HRK\":4.6041,\"HUF\":202.2,\"IDR\":10729.0,\"ILS\":2.5829,\"INR\":51.848,\"ISK\":79.149,\"JPY\":80.232,\"KRW\":808.07,\"MXN\":13.851,\"MYR\":2.9801,\"NOK\":6.0544,\"NZD\":1.092,\"PHP\":38.764,\"PLN\":2.6744,\"RON\":2.8727,\"RUB\":49.282,\"SEK\":6.5589,\"SGD\":0.99088,\"THB\":23.615,\"TRY\":4.7243,\"USD\":0.72053,\"ZAR\":11.038,\"EUR\":0.61932}}";
    private static final String HRK_RATES_DATA = "{\"base\":\"CAD\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.0519,\"BGN\":1.2727,\"BRL\":3.1182,\"CHF\":0.73369,\"CNY\":5.1702,\"CZK\":16.734,\"DKK\":4.8524,\"GBP\":0.58452,\"HKD\":5.9428,\"HRK\":4.8377,\"HUF\":212.46,\"IDR\":11273.0,\"ILS\":2.714,\"INR\":54.478,\"ISK\":83.162,\"JPY\":84.303,\"KRW\":849.03,\"MXN\":14.554,\"MYR\":3.1313,\"NOK\":6.3617,\"NZD\":1.1474,\"PHP\":40.732,\"PLN\":2.8101,\"RON\":3.0185,\"RUB\":51.783,\"SEK\":6.8916,\"SGD\":1.0412,\"THB\":24.813,\"TRY\":4.964,\"USD\":0.7571,\"ZAR\":11.598,\"EUR\":0.65074}}";
    private static final String SGD_RATES_DATA = "{\"base\":\"DKK\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.21638,\"BGN\":0.26182,\"BRL\":0.64147,\"CAD\":0.20533,\"CHF\":0.15094,\"CNY\":1.0636,\"CZK\":3.4424,\"GBP\":0.12025,\"HKD\":1.2225,\"HRK\":0.99519,\"HUF\":43.707,\"IDR\":2319.1,\"ILS\":0.55831,\"INR\":11.207,\"ISK\":17.108,\"JPY\":17.343,\"KRW\":174.66,\"MXN\":2.9941,\"MYR\":0.64418,\"NOK\":1.3087,\"NZD\":0.23606,\"PHP\":8.3791,\"PLN\":0.57808,\"RON\":0.62096,\"RUB\":10.653,\"SEK\":1.4178,\"SGD\":0.2142,\"THB\":5.1044,\"TRY\":1.0212,\"USD\":0.15574,\"ZAR\":2.386,\"EUR\":0.13387}}";
    private static final String TRY_RATES_DATA = "{\"base\":\"CZK\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":0.063146,\"BGN\":0.076406,\"BRL\":0.1872,\"CAD\":0.059918,\"CHF\":0.044048,\"CNY\":0.31038,\"DKK\":0.2913,\"GBP\":0.035091,\"HKD\":0.35677,\"HRK\":0.29042,\"HUF\":12.755,\"IDR\":676.76,\"ILS\":0.16293,\"INR\":3.2705,\"ISK\":4.9928,\"JPY\":5.0608,\"KRW\":50.97,\"MXN\":0.87373,\"MYR\":0.18799,\"NOK\":0.38191,\"NZD\":0.068886,\"PHP\":2.4452,\"PLN\":0.1687,\"RON\":0.18121,\"RUB\":3.1086,\"SEK\":0.41373,\"SGD\":0.062504,\"THB\":1.4896,\"TRY\":0.298,\"USD\":0.045448,\"ZAR\":0.69628,\"EUR\":0.039066}}";

    private static final int NUMBER_OF_DECIMALS = 2;
    @NonNull
    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
    private static final boolean STRIP_TRAILING_ZEROS = true;

    @NonNull
    private final ExchangeCalculator exchangeCalculator = new CurrencyExchangeCalculator("EUR", BigDecimal.ONE);
    @NonNull
    private final CurrencyFormatter currencyFormatter = new CurrencyFormatterProvider(NUMBER_OF_DECIMALS, ROUNDING_MODE, STRIP_TRAILING_ZEROS);

    @Mock
    private ExchangeRatesLiveData.OnLiveRatesDataListener onLiveRatesDataListener;

    @NonNull
    private final Gson gson = new GsonBuilder()
            .registerTypeAdapter(ExchangeRatesResponse.class, new ExchangeRatesResponseDeserializer())
            .registerTypeAdapter(new TypeToken<List<Rate>>() {
            }.getType(), new RatesDeserializer(exchangeCalculator, currencyFormatter))
            .create();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void setRatesData_test() throws InterruptedException {
        // Given
        RatesLiveData ratesLiveData = new ExchangeRatesLiveData(onLiveRatesDataListener);

        List<Rate> eurRatesList = gson.fromJson(EUR_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> bgnRatesList = gson.fromJson(BGN_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> audRatesList = gson.fromJson(AUD_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> cadRatesList = gson.fromJson(CAD_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> dkkRatesList = gson.fromJson(DKK_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> czkRatesList = gson.fromJson(CZK_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> myrRatesList = gson.fromJson(MYR_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> ronRatesList = gson.fromJson(RON_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> phpRatesList = gson.fromJson(PHP_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> hrkRatesList = gson.fromJson(HRK_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> sgdRatesList = gson.fromJson(SGD_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> tryRatesList = gson.fromJson(TRY_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

        Runnable runnableTask1 = new SetLiveRatesDataRunnable(ratesLiveData, eurRatesList);
        Runnable runnableTask2 = new SetLiveRatesDataRunnable(ratesLiveData, bgnRatesList);
        Runnable runnableTask3 = new SetLiveRatesDataRunnable(ratesLiveData, audRatesList);
        Runnable runnableTask4 = new SetLiveRatesDataRunnable(ratesLiveData, cadRatesList);
        Runnable runnableTask5 = new SetLiveRatesDataRunnable(ratesLiveData, dkkRatesList);
        Runnable runnableTask6 = new SetLiveRatesDataRunnable(ratesLiveData, czkRatesList);
        Runnable runnableTask7 = new SetLiveRatesDataRunnable(ratesLiveData, myrRatesList);
        Runnable runnableTask8 = new SetLiveRatesDataRunnable(ratesLiveData, ronRatesList);
        Runnable runnableTask9 = new SetLiveRatesDataRunnable(ratesLiveData, phpRatesList);
        Runnable runnableTask10 = new SetLiveRatesDataRunnable(ratesLiveData, hrkRatesList);
        Runnable runnableTask11 = new SetLiveRatesDataRunnable(ratesLiveData, sgdRatesList);
        Runnable runnableTask12 = new SetLiveRatesDataRunnable(ratesLiveData, tryRatesList);

        Callable<Void> callable1 = toCallable(runnableTask1);
        Callable<Void> callable2 = toCallable(runnableTask2);
        Callable<Void> callable3 = toCallable(runnableTask3);
        Callable<Void> callable4 = toCallable(runnableTask4);
        Callable<Void> callable5 = toCallable(runnableTask5);
        Callable<Void> callable6 = toCallable(runnableTask6);
        Callable<Void> callable7 = toCallable(runnableTask7);
        Callable<Void> callable8 = toCallable(runnableTask8);
        Callable<Void> callable9 = toCallable(runnableTask9);
        Callable<Void> callable10 = toCallable(runnableTask10);
        Callable<Void> callable11 = toCallable(runnableTask11);
        Callable<Void> callable12 = toCallable(runnableTask12);

        List<Callable<Void>> callableList = new ArrayList<>();
        callableList.add(callable1);
        callableList.add(callable2);
        callableList.add(callable3);
        callableList.add(callable4);
        callableList.add(callable5);
        callableList.add(callable6);
        callableList.add(callable7);
        callableList.add(callable8);
        callableList.add(callable9);
        callableList.add(callable10);
        callableList.add(callable11);
        callableList.add(callable12);

        // When
        executorService.invokeAll(callableList);

        // Then
        ArgumentCaptor<Integer> fromPositionArgumentCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> toPositionArgumentCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(onLiveRatesDataListener, times(1)).notifyDataSetChanged();
        Mockito.verify(onLiveRatesDataListener, times(11)).notifyItemRangeChanged(fromPositionArgumentCaptor.capture(), toPositionArgumentCaptor.capture());
        Mockito.verify(onLiveRatesDataListener, never()).notifyItemMoved(isA(Integer.class), isA(Integer.class), isA(String.class), isA(BigDecimal.class));
        assertEquals(1L, (long) fromPositionArgumentCaptor.getValue());
        assertEquals(33, (long) toPositionArgumentCaptor.getValue());
    }

    @Test
    public void swapPositions_test() throws InterruptedException {
        // Given
        RatesLiveData ratesLiveData = new ExchangeRatesLiveData(onLiveRatesDataListener);
        List<Rate> eurRatesList = gson.fromJson(EUR_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        ratesLiveData.setRatesList(eurRatesList);

        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

        Runnable runnableTask1 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 2);
        Runnable runnableTask2 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 6);
        Runnable runnableTask3 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 4);
        Runnable runnableTask4 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 8);
        Runnable runnableTask5 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 3);
        Runnable runnableTask6 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 2);
        Runnable runnableTask7 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 4);
        Runnable runnableTask8 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 6);
        Runnable runnableTask9 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 12);
        Runnable runnableTask10 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 21);
        Runnable runnableTask11 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 11);
        Runnable runnableTask12 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 22);
        Runnable runnableTask13 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 10);
        Runnable runnableTask14 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 7);
        Runnable runnableTask15 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 0);

        Callable<Void> callable1 = toCallable(runnableTask1);
        Callable<Void> callable2 = toCallable(runnableTask2);
        Callable<Void> callable3 = toCallable(runnableTask3);
        Callable<Void> callable4 = toCallable(runnableTask4);
        Callable<Void> callable5 = toCallable(runnableTask5);
        Callable<Void> callable6 = toCallable(runnableTask6);
        Callable<Void> callable7 = toCallable(runnableTask7);
        Callable<Void> callable8 = toCallable(runnableTask8);
        Callable<Void> callable9 = toCallable(runnableTask9);
        Callable<Void> callable10 = toCallable(runnableTask10);
        Callable<Void> callable11 = toCallable(runnableTask11);
        Callable<Void> callable12 = toCallable(runnableTask12);
        Callable<Void> callable13 = toCallable(runnableTask13);
        Callable<Void> callable14 = toCallable(runnableTask14);
        Callable<Void> callable15 = toCallable(runnableTask15);

        List<Callable<Void>> callableList = new ArrayList<>();
        callableList.add(callable1);
        callableList.add(callable2);
        callableList.add(callable3);
        callableList.add(callable4);
        callableList.add(callable5);
        callableList.add(callable6);
        callableList.add(callable7);
        callableList.add(callable8);
        callableList.add(callable9);
        callableList.add(callable10);
        callableList.add(callable11);
        callableList.add(callable12);
        callableList.add(callable13);
        callableList.add(callable14);
        callableList.add(callable15);

        // When
        executorService.invokeAll(callableList);

        // Then
        Mockito.verify(onLiveRatesDataListener, times(1)).notifyDataSetChanged();
        Mockito.verify(onLiveRatesDataListener, never()).notifyItemRangeChanged(isA(Integer.class), isA(Integer.class));
        Mockito.verify(onLiveRatesDataListener, times(15)).notifyItemMoved(isA(Integer.class), isA(Integer.class), isA(String.class), isA(BigDecimal.class));
    }

    @Test
    public void setRatesData_and_SwapPositions_test() throws InterruptedException {
        // Given
        RatesLiveData ratesLiveData = new ExchangeRatesLiveData(onLiveRatesDataListener);
        List<Rate> eurRatesList = gson.fromJson(EUR_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        ratesLiveData.setRatesList(eurRatesList);

        ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

        Runnable runnableTask1 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 2);
        Runnable runnableTask2 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 6);
        Runnable runnableTask3 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 4);
        Runnable runnableTask4 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 8);
        Runnable runnableTask5 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 3);
        Runnable runnableTask6 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 2);
        Runnable runnableTask7 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 4);
        Runnable runnableTask8 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 6);
        Runnable runnableTask9 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 12);
        Runnable runnableTask10 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 21);
        Runnable runnableTask11 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 11);
        Runnable runnableTask12 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 22);
        Runnable runnableTask13 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 10);
        Runnable runnableTask14 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 7);
        Runnable runnableTask15 = new SwapItemsInLiveRatesDataRunnable(ratesLiveData, 0);

        List<Rate> bgnRatesList = gson.fromJson(BGN_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> audRatesList = gson.fromJson(AUD_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> cadRatesList = gson.fromJson(CAD_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> dkkRatesList = gson.fromJson(DKK_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> czkRatesList = gson.fromJson(CZK_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> myrRatesList = gson.fromJson(MYR_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> ronRatesList = gson.fromJson(RON_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> phpRatesList = gson.fromJson(PHP_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> hrkRatesList = gson.fromJson(HRK_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> sgdRatesList = gson.fromJson(SGD_RATES_DATA, ExchangeRatesResponse.class).getRatesList();
        List<Rate> tryRatesList = gson.fromJson(TRY_RATES_DATA, ExchangeRatesResponse.class).getRatesList();

        Runnable runnableTask16 = new SetLiveRatesDataRunnable(ratesLiveData, bgnRatesList);
        Runnable runnableTask17 = new SetLiveRatesDataRunnable(ratesLiveData, audRatesList);
        Runnable runnableTask18 = new SetLiveRatesDataRunnable(ratesLiveData, cadRatesList);
        Runnable runnableTask19 = new SetLiveRatesDataRunnable(ratesLiveData, dkkRatesList);
        Runnable runnableTask20 = new SetLiveRatesDataRunnable(ratesLiveData, czkRatesList);
        Runnable runnableTask21 = new SetLiveRatesDataRunnable(ratesLiveData, myrRatesList);
        Runnable runnableTask22 = new SetLiveRatesDataRunnable(ratesLiveData, ronRatesList);
        Runnable runnableTask23 = new SetLiveRatesDataRunnable(ratesLiveData, phpRatesList);
        Runnable runnableTask24 = new SetLiveRatesDataRunnable(ratesLiveData, hrkRatesList);
        Runnable runnableTask25 = new SetLiveRatesDataRunnable(ratesLiveData, sgdRatesList);
        Runnable runnableTask26 = new SetLiveRatesDataRunnable(ratesLiveData, tryRatesList);

        Callable<Void> callable1 = toCallable(runnableTask1);
        Callable<Void> callable2 = toCallable(runnableTask2);
        Callable<Void> callable3 = toCallable(runnableTask3);
        Callable<Void> callable4 = toCallable(runnableTask4);
        Callable<Void> callable5 = toCallable(runnableTask5);
        Callable<Void> callable6 = toCallable(runnableTask6);
        Callable<Void> callable7 = toCallable(runnableTask7);
        Callable<Void> callable8 = toCallable(runnableTask8);
        Callable<Void> callable9 = toCallable(runnableTask9);
        Callable<Void> callable10 = toCallable(runnableTask10);
        Callable<Void> callable11 = toCallable(runnableTask11);
        Callable<Void> callable12 = toCallable(runnableTask12);
        Callable<Void> callable13 = toCallable(runnableTask13);
        Callable<Void> callable14 = toCallable(runnableTask14);
        Callable<Void> callable15 = toCallable(runnableTask15);
        Callable<Void> callable16 = toCallable(runnableTask16);
        Callable<Void> callable17 = toCallable(runnableTask17);
        Callable<Void> callable18 = toCallable(runnableTask18);
        Callable<Void> callable19 = toCallable(runnableTask19);
        Callable<Void> callable20 = toCallable(runnableTask20);
        Callable<Void> callable21 = toCallable(runnableTask21);
        Callable<Void> callable22 = toCallable(runnableTask22);
        Callable<Void> callable23 = toCallable(runnableTask23);
        Callable<Void> callable24 = toCallable(runnableTask24);
        Callable<Void> callable25 = toCallable(runnableTask25);
        Callable<Void> callable26 = toCallable(runnableTask26);

        List<Callable<Void>> callableList = new ArrayList<>();
        callableList.add(callable1);
        callableList.add(callable2);
        callableList.add(callable3);
        callableList.add(callable4);
        callableList.add(callable5);
        callableList.add(callable6);
        callableList.add(callable7);
        callableList.add(callable8);
        callableList.add(callable9);
        callableList.add(callable10);
        callableList.add(callable11);
        callableList.add(callable12);
        callableList.add(callable13);
        callableList.add(callable14);
        callableList.add(callable15);
        callableList.add(callable16);
        callableList.add(callable17);
        callableList.add(callable18);
        callableList.add(callable19);
        callableList.add(callable20);
        callableList.add(callable21);
        callableList.add(callable22);
        callableList.add(callable23);
        callableList.add(callable24);
        callableList.add(callable25);
        callableList.add(callable26);

        // When
        executorService.invokeAll(callableList);

        // Then
        ArgumentCaptor<Integer> fromPositionArgumentCaptor = ArgumentCaptor.forClass(Integer.class);
        ArgumentCaptor<Integer> toPositionArgumentCaptor = ArgumentCaptor.forClass(Integer.class);
        Mockito.verify(onLiveRatesDataListener, times(1)).notifyDataSetChanged();
        Mockito.verify(onLiveRatesDataListener, times(11)).notifyItemRangeChanged(fromPositionArgumentCaptor.capture(), toPositionArgumentCaptor.capture());
        Mockito.verify(onLiveRatesDataListener, times(15)).notifyItemMoved(isA(Integer.class), isA(Integer.class), isA(String.class), isA(BigDecimal.class));
        assertEquals(1L, (long) fromPositionArgumentCaptor.getValue());
        assertEquals(33, (long) toPositionArgumentCaptor.getValue());
    }

    @NonNull
    private static Callable<Void> toCallable(@NonNull Runnable runnable) {
        return () -> {
            runnable.run();
            return null;
        };
    }

    private final class SetLiveRatesDataRunnable implements Runnable {

        @NonNull
        private final RatesLiveData ratesLiveData;
        @NonNull
        private final List<Rate> ratesList;

        SetLiveRatesDataRunnable(@NonNull RatesLiveData ratesLiveData, @NonNull List<Rate> ratesList) {
            this.ratesLiveData = ratesLiveData;
            this.ratesList = ratesList;
        }

        @Override
        public void run() {
            ratesLiveData.setRatesList(ratesList);
        }
    }

    private final class SwapItemsInLiveRatesDataRunnable implements Runnable {

        @NonNull
        private final RatesLiveData ratesLiveData;
        private final int position;

        SwapItemsInLiveRatesDataRunnable(@NonNull RatesLiveData ratesLiveData, int position) {
            this.ratesLiveData = ratesLiveData;
            this.position = position;
        }

        @Override
        public void run() {
            ratesLiveData.swapPositionWithHeader(position);
        }
    }
}