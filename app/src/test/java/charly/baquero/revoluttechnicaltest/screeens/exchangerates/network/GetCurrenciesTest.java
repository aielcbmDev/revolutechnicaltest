package charly.baquero.revoluttechnicaltest.screeens.exchangerates.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.CurrencyExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.ExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter.CurrencyFormatter;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter.CurrencyFormatterProvider;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.deserialaizers.ExchangeRatesResponseDeserializer;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.deserialaizers.RatesDeserializer;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.ExchangeRatesResponse;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.observers.TestObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.HttpException;

import static org.junit.Assert.assertEquals;

public final class GetCurrenciesTest {
    private static final String SUCCESSFUL_GET_CURRENCIES_EUR = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6205,\"BGN\":1.9608,\"BRL\":4.804,\"CAD\":1.5377,\"CHF\":1.1304,\"CNY\":7.9654,\"CZK\":25.781,\"DKK\":7.4757,\"GBP\":0.90053,\"HKD\":9.1557,\"HRK\":7.4531,\"HUF\":327.32,\"IDR\":17368.0,\"ILS\":4.1812,\"INR\":83.931,\"ISK\":128.13,\"JPY\":129.88,\"KRW\":1308.1,\"MXN\":22.422,\"MYR\":4.8243,\"NOK\":9.8009,\"NZD\":1.7678,\"PHP\":62.752,\"PLN\":4.3293,\"RON\":4.6503,\"RUB\":79.778,\"SEK\":10.618,\"SGD\":1.6041,\"THB\":38.227,\"TRY\":7.6477,\"USD\":1.1664,\"ZAR\":17.869}}";
    private static final int NUMBER_OF_DECIMALS = 2;
    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
    private static final boolean STRIP_TRAILING_ZEROS = true;

    private MockWebServer mockWebServer;
    private GetExchangeRateDataSource getExchangeRateDataSource;
    private final ExchangeCalculator exchangeCalculator = new CurrencyExchangeCalculator("EUR", BigDecimal.ONE);
    private final CurrencyFormatter currencyFormatter = new CurrencyFormatterProvider(NUMBER_OF_DECIMALS, ROUNDING_MODE, STRIP_TRAILING_ZEROS);

    private final Gson gson = new GsonBuilder()
            .registerTypeAdapter(ExchangeRatesResponse.class, new ExchangeRatesResponseDeserializer())
            .registerTypeAdapter(new TypeToken<List<Rate>>() {
            }.getType(), new RatesDeserializer(exchangeCalculator, currencyFormatter))
            .create();

    @Before
    public void before() throws IOException {
        RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        HttpUrl httpUrl = mockWebServer.url("/");
        getExchangeRateDataSource = new GetExchangeRate(okHttpClient, httpUrl, gson);
    }

    @After
    public void tearDown() {
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
    }

    @Test
    public void getExchangeRate_EUR_test() {
        // Given
        exchangeCalculator.setDataToConvert("EUR", BigDecimal.TEN);
        mockWebServer.enqueue(new MockResponse().setBody(SUCCESSFUL_GET_CURRENCIES_EUR));

        // When
        String currencyNameToConvert = exchangeCalculator.getCurrencyNameToConvert();
        Single<ExchangeRatesResponse> getExchangeRateSingle = getExchangeRateDataSource.getExchangeRateForCurrency(currencyNameToConvert);
        TestObserver<ExchangeRatesResponse> testObserver = getExchangeRateSingle.test();

        // Then
        testObserver.assertTerminated()
                .assertNoErrors()
                .assertValueCount(1);

        List<List<Object>> eventList = testObserver.getEvents();
        ExchangeRatesResponse exchangeRatesResponse = (ExchangeRatesResponse) eventList.get(0).get(0);
        assertEquals("EUR", exchangeRatesResponse.getBase());
        assertEquals("2018-09-06", exchangeRatesResponse.getDate());
        List<Rate> ratesList = exchangeRatesResponse.getRatesList();
        assertEquals(33, ratesList.size());

        Rate rate0 = ratesList.get(0);
        assertEquals("EUR", rate0.getName());
        assertEquals(0, BigDecimal.TEN.compareTo(rate0.getRate()));
        assertEquals(0, new BigDecimal("100").compareTo(rate0.getConvertedAmount()));
        assertEquals("100", rate0.getFormattedConvertedAmount());

        Rate rate1 = ratesList.get(1);
        assertEquals("AUD", rate1.getName());
        assertEquals(0, new BigDecimal("1.6205").compareTo(rate1.getRate()));
        assertEquals(0, new BigDecimal("16.205").compareTo(rate1.getConvertedAmount()));
        assertEquals("16.2", rate1.getFormattedConvertedAmount());

        Rate rate2 = ratesList.get(2);
        assertEquals("BGN", rate2.getName());
        assertEquals(0, new BigDecimal("1.9608").compareTo(rate2.getRate()));
        assertEquals(0, new BigDecimal("19.608").compareTo(rate2.getConvertedAmount()));
        assertEquals("19.61", rate2.getFormattedConvertedAmount());

        Rate rate3 = ratesList.get(3);
        assertEquals("BRL", rate3.getName());
        assertEquals(0, new BigDecimal("4.804").compareTo(rate3.getRate()));
        assertEquals(0, new BigDecimal("48.04").compareTo(rate3.getConvertedAmount()));
        assertEquals("48.04", rate3.getFormattedConvertedAmount());

        Rate rate4 = ratesList.get(4);
        assertEquals("CAD", rate4.getName());
        assertEquals(0, new BigDecimal("1.5377").compareTo(rate4.getRate()));
        assertEquals(0, new BigDecimal("15.377").compareTo(rate4.getConvertedAmount()));
        assertEquals("15.38", rate4.getFormattedConvertedAmount());

        Rate rate5 = ratesList.get(5);
        assertEquals("CHF", rate5.getName());
        assertEquals(0, new BigDecimal("1.1304").compareTo(rate5.getRate()));
        assertEquals(0, new BigDecimal("11.304").compareTo(rate5.getConvertedAmount()));
        assertEquals("11.3", rate5.getFormattedConvertedAmount());

        Rate rate6 = ratesList.get(6);
        assertEquals("CNY", rate6.getName());
        assertEquals(0, new BigDecimal("7.9654").compareTo(rate6.getRate()));
        assertEquals(0, new BigDecimal("79.654").compareTo(rate6.getConvertedAmount()));
        assertEquals("79.65", rate6.getFormattedConvertedAmount());

        Rate rate7 = ratesList.get(7);
        assertEquals("CZK", rate7.getName());
        assertEquals(0, new BigDecimal("25.781").compareTo(rate7.getRate()));
        assertEquals(0, new BigDecimal("257.81").compareTo(rate7.getConvertedAmount()));
        assertEquals("257.81", rate7.getFormattedConvertedAmount());

        Rate rate8 = ratesList.get(8);
        assertEquals("DKK", rate8.getName());
        assertEquals(0, new BigDecimal("7.4757").compareTo(rate8.getRate()));
        assertEquals(0, new BigDecimal("74.757").compareTo(rate8.getConvertedAmount()));
        assertEquals("74.76", rate8.getFormattedConvertedAmount());

        Rate rate9 = ratesList.get(9);
        assertEquals("GBP", rate9.getName());
        assertEquals(0, new BigDecimal("0.90053").compareTo(rate9.getRate()));
        assertEquals(0, new BigDecimal("9.0053").compareTo(rate9.getConvertedAmount()));
        assertEquals("9.01", rate9.getFormattedConvertedAmount());

        Rate rate10 = ratesList.get(10);
        assertEquals("HKD", rate10.getName());
        assertEquals(0, new BigDecimal("9.1557").compareTo(rate10.getRate()));
        assertEquals(0, new BigDecimal("91.557").compareTo(rate10.getConvertedAmount()));
        assertEquals("91.56", rate10.getFormattedConvertedAmount());

        Rate rate11 = ratesList.get(11);
        assertEquals("HRK", rate11.getName());
        assertEquals(0, new BigDecimal("7.4531").compareTo(rate11.getRate()));
        assertEquals(0, new BigDecimal("74.531").compareTo(rate11.getConvertedAmount()));
        assertEquals("74.53", rate11.getFormattedConvertedAmount());

        Rate rate12 = ratesList.get(12);
        assertEquals("HUF", rate12.getName());
        assertEquals(0, new BigDecimal("327.32").compareTo(rate12.getRate()));
        assertEquals(0, new BigDecimal("3273.2").compareTo(rate12.getConvertedAmount()));
        assertEquals("3273.2", rate12.getFormattedConvertedAmount());

        Rate rate13 = ratesList.get(13);
        assertEquals("IDR", rate13.getName());
        assertEquals(0, new BigDecimal("17368.0").compareTo(rate13.getRate()));
        assertEquals(0, new BigDecimal("173680").compareTo(rate13.getConvertedAmount()));
        assertEquals("173680", rate13.getFormattedConvertedAmount());

        Rate rate14 = ratesList.get(14);
        assertEquals("ILS", rate14.getName());
        assertEquals(0, new BigDecimal("4.1812").compareTo(rate14.getRate()));
        assertEquals(0, new BigDecimal("41.812").compareTo(rate14.getConvertedAmount()));
        assertEquals("41.81", rate14.getFormattedConvertedAmount());

        Rate rate15 = ratesList.get(15);
        assertEquals("INR", rate15.getName());
        assertEquals(0, new BigDecimal("83.931").compareTo(rate15.getRate()));
        assertEquals(0, new BigDecimal("839.31").compareTo(rate15.getConvertedAmount()));
        assertEquals("839.31", rate15.getFormattedConvertedAmount());

        Rate rate16 = ratesList.get(16);
        assertEquals("ISK", rate16.getName());
        assertEquals(0, new BigDecimal("128.13").compareTo(rate16.getRate()));
        assertEquals(0, new BigDecimal("1281.3").compareTo(rate16.getConvertedAmount()));
        assertEquals("1281.3", rate16.getFormattedConvertedAmount());

        Rate rate17 = ratesList.get(17);
        assertEquals("JPY", rate17.getName());
        assertEquals(0, new BigDecimal("129.88").compareTo(rate17.getRate()));
        assertEquals(0, new BigDecimal("1298.8").compareTo(rate17.getConvertedAmount()));
        assertEquals("1298.8", rate17.getFormattedConvertedAmount());

        Rate rate18 = ratesList.get(18);
        assertEquals("KRW", rate18.getName());
        assertEquals(0, new BigDecimal("1308.1").compareTo(rate18.getRate()));
        assertEquals(0, new BigDecimal("13081").compareTo(rate18.getConvertedAmount()));
        assertEquals("13081", rate18.getFormattedConvertedAmount());

        Rate rate19 = ratesList.get(19);
        assertEquals("MXN", rate19.getName());
        assertEquals(0, new BigDecimal("22.422").compareTo(rate19.getRate()));
        assertEquals(0, new BigDecimal("224.22").compareTo(rate19.getConvertedAmount()));
        assertEquals("224.22", rate19.getFormattedConvertedAmount());

        Rate rate20 = ratesList.get(20);
        assertEquals("MYR", rate20.getName());
        assertEquals(0, new BigDecimal("4.8243").compareTo(rate20.getRate()));
        assertEquals(0, new BigDecimal("48.243").compareTo(rate20.getConvertedAmount()));
        assertEquals("48.24", rate20.getFormattedConvertedAmount());

        Rate rate21 = ratesList.get(21);
        assertEquals("NOK", rate21.getName());
        assertEquals(0, new BigDecimal("9.8009").compareTo(rate21.getRate()));
        assertEquals(0, new BigDecimal("98.009").compareTo(rate21.getConvertedAmount()));
        assertEquals("98.01", rate21.getFormattedConvertedAmount());

        Rate rate22 = ratesList.get(22);
        assertEquals("NZD", rate22.getName());
        assertEquals(0, new BigDecimal("1.7678").compareTo(rate22.getRate()));
        assertEquals(0, new BigDecimal("17.678").compareTo(rate22.getConvertedAmount()));
        assertEquals("17.68", rate22.getFormattedConvertedAmount());

        Rate rate23 = ratesList.get(23);
        assertEquals("PHP", rate23.getName());
        assertEquals(0, new BigDecimal("62.752").compareTo(rate23.getRate()));
        assertEquals(0, new BigDecimal("627.52").compareTo(rate23.getConvertedAmount()));
        assertEquals("627.52", rate23.getFormattedConvertedAmount());

        Rate rate24 = ratesList.get(24);
        assertEquals("PLN", rate24.getName());
        assertEquals(0, new BigDecimal("4.3293").compareTo(rate24.getRate()));
        assertEquals(0, new BigDecimal("43.293").compareTo(rate24.getConvertedAmount()));
        assertEquals("43.29", rate24.getFormattedConvertedAmount());

        Rate rate25 = ratesList.get(25);
        assertEquals("RON", rate25.getName());
        assertEquals(0, new BigDecimal("4.6503").compareTo(rate25.getRate()));
        assertEquals(0, new BigDecimal("46.503").compareTo(rate25.getConvertedAmount()));
        assertEquals("46.5", rate25.getFormattedConvertedAmount());

        Rate rate26 = ratesList.get(26);
        assertEquals("RUB", rate26.getName());
        assertEquals(0, new BigDecimal("79.778").compareTo(rate26.getRate()));
        assertEquals(0, new BigDecimal("797.78").compareTo(rate26.getConvertedAmount()));
        assertEquals("797.78", rate26.getFormattedConvertedAmount());

        Rate rate27 = ratesList.get(27);
        assertEquals("SEK", rate27.getName());
        assertEquals(0, new BigDecimal("10.618").compareTo(rate27.getRate()));
        assertEquals(0, new BigDecimal("106.18").compareTo(rate27.getConvertedAmount()));
        assertEquals("106.18", rate27.getFormattedConvertedAmount());

        Rate rate28 = ratesList.get(28);
        assertEquals("SGD", rate28.getName());
        assertEquals(0, new BigDecimal("1.6041").compareTo(rate28.getRate()));
        assertEquals(0, new BigDecimal("16.041").compareTo(rate28.getConvertedAmount()));
        assertEquals("16.04", rate28.getFormattedConvertedAmount());

        Rate rate29 = ratesList.get(29);
        assertEquals("THB", rate29.getName());
        assertEquals(0, new BigDecimal("38.227").compareTo(rate29.getRate()));
        assertEquals(0, new BigDecimal("382.27").compareTo(rate29.getConvertedAmount()));
        assertEquals("382.27", rate29.getFormattedConvertedAmount());

        Rate rate30 = ratesList.get(30);
        assertEquals("TRY", rate30.getName());
        assertEquals(0, new BigDecimal("7.6477").compareTo(rate30.getRate()));
        assertEquals(0, new BigDecimal("76.477").compareTo(rate30.getConvertedAmount()));
        assertEquals("76.48", rate30.getFormattedConvertedAmount());

        Rate rate31 = ratesList.get(31);
        assertEquals("USD", rate31.getName());
        assertEquals(0, new BigDecimal("1.1664").compareTo(rate31.getRate()));
        assertEquals(0, new BigDecimal("11.664").compareTo(rate31.getConvertedAmount()));
        assertEquals("11.66", rate31.getFormattedConvertedAmount());

        Rate rate32 = ratesList.get(32);
        assertEquals("ZAR", rate32.getName());
        assertEquals(0, new BigDecimal("17.869").compareTo(rate32.getRate()));
        assertEquals(0, new BigDecimal("178.69").compareTo(rate32.getConvertedAmount()));
        assertEquals("178.69", rate32.getFormattedConvertedAmount());
    }

    @Test
    public void test_getExchangeRate_parsingError() {
        // Given
        TestObserver<ExchangeRatesResponse> testObserver = new TestObserver<>();
        mockWebServer.enqueue(new MockResponse().setResponseCode(200).setBody("any response!!"));

        // When
        Single<ExchangeRatesResponse> getExchangeRateSingle = getExchangeRateDataSource.getExchangeRateForCurrency("EUR");
        getExchangeRateSingle.subscribe(testObserver);

        // Then
        testObserver.assertSubscribed();
        testObserver.assertError(JsonSyntaxException.class);
    }

    @Test
    public void test_getExchangeRate_BadRequest() {
        // Given
        TestObserver<ExchangeRatesResponse> testObserver = new TestObserver<>();
        mockWebServer.enqueue(new MockResponse().setResponseCode(400).setBody("any response!!"));

        // When
        Single<ExchangeRatesResponse> getExchangeRateSingle = getExchangeRateDataSource.getExchangeRateForCurrency("EUR");
        getExchangeRateSingle.subscribe(testObserver);

        // Then
        testObserver.assertSubscribed();
        testObserver.assertError(HttpException.class);
        List<List<Object>> eventsReceived = testObserver.getEvents();
        List<Object> potentialErrorsReceived = eventsReceived.get(1);
        HttpException httpException = (HttpException) potentialErrorsReceived.get(0);
        assertEquals(400, httpException.code());
    }

    @Test
    public void test_getExchangeRate_InternalServerError() {
        // Given
        TestObserver<ExchangeRatesResponse> testObserver = new TestObserver<>();
        mockWebServer.enqueue(new MockResponse().setResponseCode(500).setBody("any response!!"));

        // When
        Single<ExchangeRatesResponse> getExchangeRateSingle = getExchangeRateDataSource.getExchangeRateForCurrency("EUR");
        getExchangeRateSingle.subscribe(testObserver);

        // Then
        testObserver.assertSubscribed();
        testObserver.assertError(HttpException.class);
        List<List<Object>> eventsReceived = testObserver.getEvents();
        List<Object> potentialErrorsReceived = eventsReceived.get(1);
        HttpException httpException = (HttpException) potentialErrorsReceived.get(0);
        assertEquals(500, httpException.code());
    }
}