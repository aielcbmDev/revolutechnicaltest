package charly.baquero.revoluttechnicaltest;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.ExchangeRatesFragment;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

public final class MainActivityPresenterTest {

    @Mock
    private MainActivityContract.View mainActivityView;

    @Captor
    private ArgumentCaptor<Class<? extends Fragment>> classArgumentCaptor;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void onCreate_withNull_Bundle() {
        // Given
        MainActivityContract.Presenter mainActivityPresenter = new MainActivityPresenter(mainActivityView);

        // When
        mainActivityPresenter.onCreate(null);

        // Then
        Mockito.verify(mainActivityView, times(1)).changeFragment(classArgumentCaptor.capture());
        assertEquals(ExchangeRatesFragment.class, classArgumentCaptor.getValue());
    }

    @Test
    public void onCreate_withNonNull_Bundle() {
        // Given
        MainActivityContract.Presenter mainActivityPresenter = new MainActivityPresenter(mainActivityView);
        Bundle savedInstanceState = new Bundle();


        // When
        mainActivityPresenter.onCreate(savedInstanceState);

        // Then
        Mockito.verify(mainActivityView, never()).changeFragment(classArgumentCaptor.capture());
    }

    @Test
    public void onBackPressed_with_1_or_less_fragmentOnBackStack() {
        // Given
        MainActivityContract.Presenter mainActivityPresenter = new MainActivityPresenter(mainActivityView);

        // When
        mainActivityPresenter.onBackPressed(1);

        // Then
        Mockito.verify(mainActivityView, times(1)).performFinish();
    }

    @Test
    public void onBackPressed_with_more_than_1_fragmentOnBackStack() {
        // Given
        MainActivityContract.Presenter mainActivityPresenter = new MainActivityPresenter(mainActivityView);

        // When
        mainActivityPresenter.onBackPressed(2);

        // Then
        Mockito.verify(mainActivityView, times(1)).performBackPressed();
    }
}