package charly.baquero.revoluttechnicaltest.screeens.exchangerates.network;

import androidx.annotation.NonNull;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.ExchangeRatesResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

interface GetExchangeRateService {

    @NonNull
    @Headers("Content-Type: application/json")
    @GET("latest")
    Single<ExchangeRatesResponse> getExchangeRateForCurrency(@NonNull @Query("base") String currency);
}
