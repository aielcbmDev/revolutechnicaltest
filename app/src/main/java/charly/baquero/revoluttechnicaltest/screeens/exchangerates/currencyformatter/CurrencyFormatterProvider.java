package charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter;

import androidx.annotation.NonNull;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class CurrencyFormatterProvider implements CurrencyFormatter {

    private final int numberOfDecimals;
    @NonNull
    private final RoundingMode roundingMode;
    private final boolean stripTrailingZeros;

    public CurrencyFormatterProvider(int numberOfDecimals, @NonNull RoundingMode roundingMode, boolean stripTrailingZeros) {
        this.numberOfDecimals = numberOfDecimals;
        this.roundingMode = roundingMode;
        this.stripTrailingZeros = stripTrailingZeros;
    }

    @NonNull
    @Override
    public String formatAmount(@NonNull BigDecimal amount) {
        BigDecimal scaledAmount = amount.setScale(numberOfDecimals, roundingMode);
        if (stripTrailingZeros) {
            BigDecimal formattedAmount = scaledAmount.stripTrailingZeros();
            return formattedAmount.toPlainString();
        } else {
            return scaledAmount.toPlainString();
        }
    }
}
