package charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter;

import androidx.annotation.NonNull;

import java.math.BigDecimal;

public interface CurrencyFormatter {

    @NonNull
    String formatAmount(@NonNull BigDecimal amount);
}
