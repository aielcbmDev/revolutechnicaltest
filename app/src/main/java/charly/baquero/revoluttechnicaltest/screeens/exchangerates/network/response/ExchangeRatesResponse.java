package charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public final class ExchangeRatesResponse {

    @NonNull
    @SerializedName("base")
    private final String base;
    @NonNull
    @SerializedName("date")
    private final String date;
    @NonNull
    @SerializedName("rates")
    private final List<Rate> ratesList;

    public ExchangeRatesResponse(@NonNull String base, @NonNull String date, @NonNull List<Rate> ratesList) {
        this.base = base;
        this.date = date;
        this.ratesList = ratesList;
    }

    @NonNull
    public String getBase() {
        return base;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    @NonNull
    public List<Rate> getRatesList() {
        return ratesList;
    }
}
