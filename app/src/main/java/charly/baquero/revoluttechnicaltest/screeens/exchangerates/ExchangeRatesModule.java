package charly.baquero.revoluttechnicaltest.screeens.exchangerates;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import charly.baquero.revoluttechnicaltest.BuildConfig;
import charly.baquero.revoluttechnicaltest.di.scopes.PerFragment;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.ExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.livedata.ExchangeRatesLiveData;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.livedata.RatesLiveData;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.GetExchangeRate;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.GetExchangeRateDataSource;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.settings.ExchangeRateRequestSettings;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.settings.RequestSettings;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
public final class ExchangeRatesModule {

    @NonNull
    @Provides
    @PerFragment
    ExchangeRatesContract.View provideExchangeRateFragmentView(@NonNull ExchangeRatesFragment exchangeRatesFragment) {
        return exchangeRatesFragment;
    }

    @NonNull
    @Provides
    @PerFragment
    RequestSettings provideRequestSettings() {
        return new ExchangeRateRequestSettings(0L, 1L, TimeUnit.SECONDS, 3, 1L, TimeUnit.SECONDS, 500L, TimeUnit.MILLISECONDS);
    }

    @NonNull
    @Provides
    @PerFragment
    GetExchangeRateDataSource provideGetExchangeRateDataSource(@NonNull OkHttpClient okHttpClient, @NonNull Gson gson) {
        return new GetExchangeRate(okHttpClient, BuildConfig.BASE_URL, gson);
    }

    @NonNull
    @Provides
    @PerFragment
    RatesLiveData provideLiveRatesData(@NonNull ExchangeRatesContract.View exchangeRateFragmentView) {
        return new ExchangeRatesLiveData(exchangeRateFragmentView);
    }

    @NonNull
    @Provides
    @PerFragment
    ExchangeRatesContract.Presenter provideExchangeRateFragmentPresenter(@NonNull ExchangeRatesContract.View exchangeRateFragmentView, @NonNull GetExchangeRateDataSource getExchangeRateDataSource, @NonNull ExchangeCalculator exchangeCalculator, @NonNull RequestSettings requestSettings, @NonNull RatesLiveData ratesLiveData) {
        return new ExchangeRatesPresenter(exchangeRateFragmentView, getExchangeRateDataSource, exchangeCalculator, requestSettings, ratesLiveData);
    }

    @NonNull
    @Provides
    @PerFragment
    ExchangeRatesAdapter provideExchangeRatesAdapter(@NonNull ExchangeRatesContract.View exchangeRateFragmentView) {
        return new ExchangeRatesAdapter(exchangeRateFragmentView, exchangeRateFragmentView, exchangeRateFragmentView);
    }
}
