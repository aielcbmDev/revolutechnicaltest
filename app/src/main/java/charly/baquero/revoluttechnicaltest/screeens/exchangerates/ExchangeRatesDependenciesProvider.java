package charly.baquero.revoluttechnicaltest.screeens.exchangerates;

import charly.baquero.revoluttechnicaltest.di.scopes.PerFragment;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ExchangeRatesDependenciesProvider {

    @ContributesAndroidInjector(modules = ExchangeRatesModule.class)
    @PerFragment
    abstract ExchangeRatesFragment provideExchangeRateFragmentFactory();
}
