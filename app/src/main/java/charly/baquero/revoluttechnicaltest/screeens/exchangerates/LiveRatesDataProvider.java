package charly.baquero.revoluttechnicaltest.screeens.exchangerates;

import androidx.annotation.NonNull;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;

interface LiveRatesDataProvider {

    @NonNull
    Rate getRateForPosition(int position);

    int getRatesItemCount();
}
