package charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators;

import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.math.BigDecimal;

public interface ExchangeCalculator extends Parcelable {

    void setDataToConvert(@NonNull String currencyName, @NonNull BigDecimal amountToConvert);

    void setAmountToConvert(@NonNull BigDecimal amountToConvert);

    @NonNull
    String getCurrencyNameToConvert();

    @NonNull
    BigDecimal getAmountToConvert();

    @NonNull
    BigDecimal calculateConvertedAmountForRate(@NonNull BigDecimal rate);
}
