package charly.baquero.revoluttechnicaltest.screeens.exchangerates.livedata;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;

public final class ExchangeRatesLiveData implements RatesLiveData {

    @NonNull
    private List<Rate> ratesList;
    @NonNull
    private final OnLiveRatesDataListener onLiveRatesDataListener;

    public ExchangeRatesLiveData(@NonNull OnLiveRatesDataListener onLiveRatesDataListener) {
        this.onLiveRatesDataListener = onLiveRatesDataListener;
        this.ratesList = Collections.emptyList();
    }

    @NonNull
    @Override
    public Rate getRateForPosition(int position) {
        return ratesList.get(position);
    }

    @Override
    public int getRatesItemCount() {
        return ratesList.size();
    }

    @Override
    public synchronized void swapPositionWithHeader(int position) {
        if (position != RecyclerView.NO_POSITION) {
            Rate rateClicked = ratesList.get(position);
            Collections.swap(ratesList, position, 0);
            onLiveRatesDataListener.notifyItemMoved(position, 0, rateClicked.getName(), rateClicked.getRate());
        }
    }

    @Override
    public synchronized void setRatesList(@NonNull List<Rate> ratesList) {
        if (this.ratesList.isEmpty()) {
            this.ratesList = ratesList;
            onLiveRatesDataListener.notifyDataSetChanged();
        } else {
            this.ratesList = ratesList;
            onLiveRatesDataListener.notifyItemRangeChanged(1, ratesList.size());
        }
    }

    @Override
    public synchronized void amountToConvertChanged() {
        if (!this.ratesList.isEmpty()) {
            for (int i = 1; i < ratesList.size(); i++) {
                Rate rate = ratesList.get(i);
                rate.calculateConvertedAmountForRate();
            }
            onLiveRatesDataListener.notifyItemRangeChanged(1, ratesList.size());
        }
    }

    @Override
    public synchronized void clearData() {
        this.ratesList.clear();
    }

    public interface OnLiveRatesDataListener {

        void notifyItemMoved(int fromPosition, int toPosition, @NonNull String currencyNameToConvert, @NonNull BigDecimal amountToConvert);

        void notifyDataSetChanged();

        void notifyItemRangeChanged(int fromPosition, int toPosition);
    }
}
