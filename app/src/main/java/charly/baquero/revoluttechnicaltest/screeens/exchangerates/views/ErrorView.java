package charly.baquero.revoluttechnicaltest.screeens.exchangerates.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import charly.baquero.revoluttechnicaltest.R;

public final class ErrorView extends RelativeLayout {

    @Nullable
    private OnErrorViewListener onErrorViewListener;

    public ErrorView(Context context) {
        super(context);
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Button retryButton = findViewById(R.id.error_view_retry_button);
        retryButton.setOnClickListener(v -> {
            if (onErrorViewListener != null) {
                onErrorViewListener.onRetryButtonClick();
            }
        });
    }

    public void setOnErrorViewListener(@NonNull OnErrorViewListener onErrorViewListener) {
        this.onErrorViewListener = onErrorViewListener;
    }

    public interface OnErrorViewListener {
        void onRetryButtonClick();
    }
}
