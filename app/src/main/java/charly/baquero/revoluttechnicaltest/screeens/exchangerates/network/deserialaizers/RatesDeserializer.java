package charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.deserialaizers;

import androidx.annotation.NonNull;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.ExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter.CurrencyFormatter;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;

public final class RatesDeserializer implements JsonDeserializer<List<Rate>> {

    @NonNull
    private final ExchangeCalculator exchangeCalculator;
    @NonNull
    private final CurrencyFormatter currencyFormatter;

    public RatesDeserializer(@NonNull ExchangeCalculator exchangeCalculator, @NonNull CurrencyFormatter currencyFormatter) {
        this.exchangeCalculator = exchangeCalculator;
        this.currencyFormatter = currencyFormatter;
    }

    @Override
    public List<Rate> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        Set<Map.Entry<String, JsonElement>> ratesEntrySet = jsonObject.entrySet();

        List<Rate> ratesList = new ArrayList<>();
        Rate defaultRate = new Rate(exchangeCalculator.getCurrencyNameToConvert(), exchangeCalculator.getAmountToConvert(), exchangeCalculator, currencyFormatter);
        ratesList.add(defaultRate);
        for (Map.Entry<String, JsonElement> rateEntry : ratesEntrySet) {
            String rateName = rateEntry.getKey();
            BigDecimal rateValue = rateEntry.getValue().getAsBigDecimal();
            Rate rate = new Rate(rateName, rateValue, exchangeCalculator, currencyFormatter);
            ratesList.add(rate);
        }

        return ratesList;
    }
}
