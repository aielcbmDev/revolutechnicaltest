package charly.baquero.revoluttechnicaltest.screeens.exchangerates.livedata;

import androidx.annotation.NonNull;

import java.util.List;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;

public interface RatesLiveData {

    @NonNull
    Rate getRateForPosition(int position);

    int getRatesItemCount();

    void swapPositionWithHeader(int position);

    void setRatesList(@NonNull List<Rate> ratesList);

    void amountToConvertChanged();

    void clearData();
}
