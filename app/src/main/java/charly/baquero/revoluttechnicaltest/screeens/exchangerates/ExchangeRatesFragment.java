package charly.baquero.revoluttechnicaltest.screeens.exchangerates;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.math.BigDecimal;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import charly.baquero.revoluttechnicaltest.R;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.views.ErrorView;
import dagger.android.support.DaggerFragment;
import io.reactivex.Observable;

public final class ExchangeRatesFragment extends DaggerFragment implements ExchangeRatesContract.View {

    @Inject
    ExchangeRatesContract.Presenter exchangeRatePresenter;
    @Inject
    ExchangeRatesAdapter exchangeRatesAdapter;
    @BindView(R.id.exchange_rate_fragment_recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.exchange_rate_fragment_progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.main_activity_error_view)
    ErrorView errorView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        exchangeRatePresenter.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.exchange_rate_fragment_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(exchangeRatesAdapter);
        errorView.setOnErrorViewListener(() -> exchangeRatePresenter.requestExchangeRatesForCurrency());
    }

    @Override
    public void onResume() {
        super.onResume();
        exchangeRatePresenter.requestExchangeRatesForCurrency();
    }

    @Override
    public void onPause() {
        super.onPause();
        exchangeRatePresenter.onPause();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        exchangeRatePresenter.onSaveInstanceState(outState);
    }

    @Override
    public void onRateClick(int position) {
        exchangeRatePresenter.onRateClick(position);
    }

    @Override
    public void notifyItemMoved(int fromPosition, int toPosition, @NonNull String currencyNameToConvert, @NonNull BigDecimal amountToConvert) {
        exchangeRatesAdapter.notifyItemMoved(fromPosition, toPosition);
        exchangeRatePresenter.updateDataToConvert(currencyNameToConvert, amountToConvert);
    }

    @Override
    public void notifyDataSetChanged() {
        exchangeRatesAdapter.notifyDataSetChanged();
    }

    @Override
    public void notifyItemRangeChanged(int fromPosition, int toPosition) {
        exchangeRatesAdapter.notifyItemRangeChanged(fromPosition, toPosition);
    }

    @NonNull
    @Override
    public Rate getRateForPosition(int position) {
        return exchangeRatePresenter.getRateForPosition(position);
    }

    @Override
    public int getRatesItemCount() {
        return exchangeRatePresenter.getRatesItemCount();
    }

    @Override
    public void onAmountToConvertObservable(@NonNull Observable<String> amountToConvertObservable) {
        exchangeRatePresenter.onAmountToConvertObservable(amountToConvertObservable);
    }

    @Override
    public void displayProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void displayErrorView() {
        errorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorView() {
        errorView.setVisibility(View.GONE);
    }

    @Override
    public void displayCurrenciesList() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideCurrenciesList() {
        recyclerView.setVisibility(View.GONE);
    }
}
