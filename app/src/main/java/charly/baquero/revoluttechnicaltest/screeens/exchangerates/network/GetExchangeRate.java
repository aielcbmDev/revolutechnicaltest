package charly.baquero.revoluttechnicaltest.screeens.exchangerates.network;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.google.gson.Gson;

import charly.baquero.revoluttechnicaltest.retrofit.SingleOnSchedulerCallAdapterFactory;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.ExchangeRatesResponse;
import io.reactivex.Single;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static io.reactivex.android.schedulers.AndroidSchedulers.mainThread;
import static io.reactivex.schedulers.Schedulers.io;

public final class GetExchangeRate implements GetExchangeRateDataSource {

    @NonNull
    private final GetExchangeRateService getExchangeRateService;

    public GetExchangeRate(@NonNull OkHttpClient sharedOkHttpClient, @NonNull String url, @NonNull Gson gson) {
        this.getExchangeRateService = provideGetExchangeRateService(sharedOkHttpClient, url, gson);
    }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    GetExchangeRate(@NonNull OkHttpClient sharedOkHttpClient, @NonNull HttpUrl httpUrl, @NonNull Gson gson) {
        this(sharedOkHttpClient, httpUrl.url().toString(), gson);
    }

    @NonNull
    @Override
    public Single<ExchangeRatesResponse> getExchangeRateForCurrency(@NonNull String base) {
        return getExchangeRateService.getExchangeRateForCurrency(base);
    }

    @NonNull
    private GetExchangeRateService provideGetExchangeRateService(@NonNull OkHttpClient sharedOkHttpClient, @NonNull String url, @NonNull Gson gson) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(SingleOnSchedulerCallAdapterFactory.create(mainThread()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(io()))
                .client(sharedOkHttpClient);
        Retrofit retrofit = builder.baseUrl(url).build();
        return retrofit.create(GetExchangeRateService.class);
    }
}
