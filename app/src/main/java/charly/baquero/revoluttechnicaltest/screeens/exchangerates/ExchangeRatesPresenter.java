package charly.baquero.revoluttechnicaltest.screeens.exchangerates;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.ExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.livedata.RatesLiveData;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.GetExchangeRateDataSource;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.ExchangeRatesResponse;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.settings.RequestSettings;
import charly.baquero.revoluttechnicaltest.utils.RetryFunction;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public final class ExchangeRatesPresenter implements ExchangeRatesContract.Presenter {

    private static final String EXCHANGE_CALCULATOR_KEY = "ExchangeRatesPresenter.EXCHANGE_CALCULATOR_KEY";

    @NonNull
    private final ExchangeRatesContract.View exchangeRateFragmentView;
    @NonNull
    private final GetExchangeRateDataSource getExchangeRateDataSource;
    @NonNull
    private final RequestSettings requestSettings;
    @NonNull
    private final RatesLiveData ratesLiveData;

    @NonNull
    private ExchangeCalculator exchangeCalculator;
    @Nullable
    private Disposable disposable;
    @NonNull
    private CompositeDisposable compositeDisposable;

    ExchangeRatesPresenter(@NonNull ExchangeRatesContract.View exchangeRateFragmentView, @NonNull GetExchangeRateDataSource getExchangeRateDataSource, @NonNull ExchangeCalculator exchangeCalculator, @NonNull RequestSettings requestSettings, @NonNull RatesLiveData ratesLiveData) {
        this.exchangeRateFragmentView = exchangeRateFragmentView;
        this.getExchangeRateDataSource = getExchangeRateDataSource;
        this.exchangeCalculator = exchangeCalculator;
        this.requestSettings = requestSettings;
        this.ratesLiveData = ratesLiveData;
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void requestExchangeRatesForCurrency() {
        regenerateCompositeDisposable();
        if (disposable != null) {
            disposable.dispose();
        }
        exchangeRateFragmentView.displayProgressBar();
        disposable = Observable.interval(requestSettings.getServerRequestInitialDelay(), requestSettings.getServerRequestPeriod(), requestSettings.getServerRequestTimeUnit())
                .flatMap((Function<Long, ObservableSource<ExchangeRatesResponse>>) aLong -> getExchangeRateDataSource.getExchangeRateForCurrency(exchangeCalculator.getCurrencyNameToConvert()).toObservable())
                .retryWhen(new RetryFunction(requestSettings.getNumberOfRetries(), requestSettings.getTimeBetweenRetries(), requestSettings.getRetriesTimeUnit()))
                .subscribeWith(new DisposableObserver<ExchangeRatesResponse>() {
                    @Override
                    public void onNext(ExchangeRatesResponse exchangeRatesResponse) {
                        List<Rate> ratesList = exchangeRatesResponse.getRatesList();
                        ratesLiveData.setRatesList(ratesList);
                        exchangeRateFragmentView.displayCurrenciesList();
                        exchangeRateFragmentView.hideProgressBar();
                        exchangeRateFragmentView.hideErrorView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError();
                    }

                    @Override
                    public void onComplete() {
                        handleError();
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void handleError() {
        exchangeRateFragmentView.hideCurrenciesList();
        exchangeRateFragmentView.hideProgressBar();
        exchangeRateFragmentView.displayErrorView();
    }

    @Override
    public void onRateClick(int position) {
        ratesLiveData.swapPositionWithHeader(position);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            exchangeCalculator = Objects.requireNonNull(savedInstanceState.getParcelable(EXCHANGE_CALCULATOR_KEY));
        }
    }

    @Override
    public void onPause() {
        compositeDisposable.dispose();
        exchangeRateFragmentView.hideCurrenciesList();
        exchangeRateFragmentView.hideErrorView();
        exchangeRateFragmentView.hideProgressBar();
        ratesLiveData.clearData();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(EXCHANGE_CALCULATOR_KEY, exchangeCalculator);
    }

    @Override
    public void updateDataToConvert(@NonNull String currencyNameToConvert, @NonNull BigDecimal amountToConvert) {
        exchangeCalculator.setDataToConvert(currencyNameToConvert, amountToConvert);
        requestExchangeRatesForCurrency();
    }

    @NonNull
    @Override
    public Rate getRateForPosition(int position) {
        return ratesLiveData.getRateForPosition(position);
    }

    @Override
    public int getRatesItemCount() {
        return ratesLiveData.getRatesItemCount();
    }

    @Override
    public void onAmountToConvertObservable(@NonNull Observable<String> amountToConvertObservable) {
        regenerateCompositeDisposable();
        Disposable disposable = amountToConvertObservable
                .debounce(requestSettings.getDebounceTime(), requestSettings.getDebounceTimeUnit())
                .filter(NumberUtils::isParsable)
                .switchMap(Observable::just)
                .map(BigDecimal::new)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<BigDecimal>() {
                    @Override
                    public void onNext(BigDecimal amountToConvert) {
                        exchangeCalculator.setAmountToConvert(amountToConvert);
                        ratesLiveData.amountToConvertChanged();
                    }

                    @Override
                    public void onError(Throwable e) {
                        // Do nothing
                    }

                    @Override
                    public void onComplete() {
                        // Do nothing
                    }
                });
        compositeDisposable.add(disposable);
    }

    private void regenerateCompositeDisposable() {
        if (compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    @NonNull
    ExchangeCalculator getExchangeCalculator() {
        return exchangeCalculator;
    }
}