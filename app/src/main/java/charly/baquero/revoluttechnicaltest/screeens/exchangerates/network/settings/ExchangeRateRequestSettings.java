package charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.settings;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

public final class ExchangeRateRequestSettings implements RequestSettings {

    private final long serverRequestInitialDelay;
    private final long serverRequestPeriod;
    @NonNull
    private final TimeUnit serverRequestTimeUnit;

    private final int numberOfRetries;
    private final long timeBetweenRetries;
    @NonNull
    private final TimeUnit retriesTimeUnit;

    private final long debounceTime;
    @NonNull
    private final TimeUnit debounceTimeUnit;

    public ExchangeRateRequestSettings(long serverRequestInitialDelay, long serverRequestPeriod, @NonNull TimeUnit serverRequestTimeUnit, int numberOfRetries, long timeBetweenRetries, @NonNull TimeUnit retriesTimeUnit, long debounceTime, @NonNull TimeUnit debounceTimeUnit) {
        this.serverRequestInitialDelay = serverRequestInitialDelay;
        this.serverRequestPeriod = serverRequestPeriod;
        this.serverRequestTimeUnit = serverRequestTimeUnit;
        this.numberOfRetries = numberOfRetries;
        this.timeBetweenRetries = timeBetweenRetries;
        this.retriesTimeUnit = retriesTimeUnit;
        this.debounceTime = debounceTime;
        this.debounceTimeUnit = debounceTimeUnit;
    }

    @Override
    public long getServerRequestInitialDelay() {
        return serverRequestInitialDelay;
    }

    @Override
    public long getServerRequestPeriod() {
        return serverRequestPeriod;
    }

    @NonNull
    @Override
    public TimeUnit getServerRequestTimeUnit() {
        return serverRequestTimeUnit;
    }

    @Override
    public int getNumberOfRetries() {
        return numberOfRetries;
    }

    @Override
    public long getTimeBetweenRetries() {
        return timeBetweenRetries;
    }

    @NonNull
    @Override
    public TimeUnit getRetriesTimeUnit() {
        return retriesTimeUnit;
    }

    @Override
    public long getDebounceTime() {
        return debounceTime;
    }

    @NonNull
    @Override
    public TimeUnit getDebounceTimeUnit() {
        return debounceTimeUnit;
    }
}
