package charly.baquero.revoluttechnicaltest.screeens.exchangerates.network;

import androidx.annotation.NonNull;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.ExchangeRatesResponse;
import io.reactivex.Single;

public interface GetExchangeRateDataSource {

    @NonNull
    Single<ExchangeRatesResponse> getExchangeRateForCurrency(@NonNull String currency);
}
