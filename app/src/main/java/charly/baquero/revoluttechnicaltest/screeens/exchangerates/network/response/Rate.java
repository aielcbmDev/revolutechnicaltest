package charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.math.BigDecimal;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.ExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter.CurrencyFormatter;

public final class Rate {

    @NonNull
    private final String name;
    @NonNull
    private final BigDecimal rate;
    @NonNull
    private final ExchangeCalculator exchangeCalculator;
    @NonNull
    private final CurrencyFormatter currencyFormatter;
    @NonNull
    private BigDecimal convertedAmount;

    public Rate(@NonNull String name, @NonNull BigDecimal rate, @NonNull ExchangeCalculator exchangeCalculator, @NonNull CurrencyFormatter currencyFormatter) {
        this.name = name;
        this.rate = rate;
        this.exchangeCalculator = exchangeCalculator;
        this.currencyFormatter = currencyFormatter;
        this.convertedAmount = exchangeCalculator.calculateConvertedAmountForRate(rate);
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public BigDecimal getRate() {
        return rate;
    }

    public void calculateConvertedAmountForRate() {
        this.convertedAmount = exchangeCalculator.calculateConvertedAmountForRate(rate);
    }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    @NonNull
    public BigDecimal getConvertedAmount() {
        return convertedAmount;
    }

    @NonNull
    public String getFormattedConvertedAmount() {
        return currencyFormatter.formatAmount(convertedAmount);
    }

    @NonNull
    public String getAmountToConvert() {
        return exchangeCalculator.getAmountToConvert().toPlainString();
    }
}
