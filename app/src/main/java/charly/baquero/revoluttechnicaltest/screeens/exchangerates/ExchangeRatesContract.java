package charly.baquero.revoluttechnicaltest.screeens.exchangerates;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.math.BigDecimal;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.livedata.ExchangeRatesLiveData;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.views.RateItemView;
import io.reactivex.Observable;

public interface ExchangeRatesContract {

    interface View extends ExchangeRatesAdapter.OnRateClickListener, ExchangeRatesLiveData.OnLiveRatesDataListener, LiveRatesDataProvider, RateItemView.OnAmountToConvertListener {

        void displayProgressBar();

        void hideProgressBar();

        void displayErrorView();

        void hideErrorView();

        void displayCurrenciesList();

        void hideCurrenciesList();
    }

    interface Presenter {

        void requestExchangeRatesForCurrency();

        void onRateClick(int position);

        void onCreate(@Nullable Bundle savedInstanceState);

        void onPause();

        void onSaveInstanceState(@NonNull Bundle outState);

        void updateDataToConvert(@NonNull String currencyNameToConvert, @NonNull BigDecimal amountToConvert);

        @NonNull
        Rate getRateForPosition(int position);

        int getRatesItemCount();

        void onAmountToConvertObservable(@NonNull Observable<String> amountToConvertObservable);
    }
}
