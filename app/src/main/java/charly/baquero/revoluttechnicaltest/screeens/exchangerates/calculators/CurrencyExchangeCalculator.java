package charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators;

import android.os.Parcel;

import androidx.annotation.NonNull;

import java.math.BigDecimal;
import java.util.Objects;

public final class CurrencyExchangeCalculator implements ExchangeCalculator {

    @NonNull
    private String currencyNameToConvert;
    @NonNull
    private BigDecimal amountToConvert;

    public CurrencyExchangeCalculator(@NonNull String currencyNameToConvert, @NonNull BigDecimal defaultAmountToConvert) {
        this.currencyNameToConvert = currencyNameToConvert;
        this.amountToConvert = defaultAmountToConvert;
    }

    private CurrencyExchangeCalculator(@NonNull Parcel in) {
        currencyNameToConvert = Objects.requireNonNull(in.readString());
        amountToConvert = (BigDecimal) Objects.requireNonNull(in.readSerializable());
    }

    public static final Creator<CurrencyExchangeCalculator> CREATOR = new Creator<CurrencyExchangeCalculator>() {
        @NonNull
        @Override
        public CurrencyExchangeCalculator createFromParcel(@NonNull Parcel in) {
            return new CurrencyExchangeCalculator(in);
        }

        @NonNull
        @Override
        public CurrencyExchangeCalculator[] newArray(int size) {
            return new CurrencyExchangeCalculator[size];
        }
    };

    @Override
    public void setDataToConvert(@NonNull String currencyName, @NonNull BigDecimal amountToConvert) {
        this.currencyNameToConvert = currencyName;
        this.amountToConvert = amountToConvert;
    }

    @Override
    public void setAmountToConvert(@NonNull BigDecimal amountToConvert) {
        this.amountToConvert = amountToConvert;
    }

    @NonNull
    @Override
    public String getCurrencyNameToConvert() {
        return currencyNameToConvert;
    }

    @NonNull
    public BigDecimal getAmountToConvert() {
        return amountToConvert;
    }

    @NonNull
    @Override
    public BigDecimal calculateConvertedAmountForRate(@NonNull BigDecimal rate) {
        return amountToConvert.multiply(rate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(currencyNameToConvert);
        dest.writeSerializable(amountToConvert);
    }
}
