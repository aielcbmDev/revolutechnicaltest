package charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.settings;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

public interface RequestSettings {

    long getServerRequestInitialDelay();

    long getServerRequestPeriod();

    @NonNull
    TimeUnit getServerRequestTimeUnit();

    int getNumberOfRetries();

    long getTimeBetweenRetries();

    @NonNull
    TimeUnit getRetriesTimeUnit();

    long getDebounceTime();

    @NonNull
    TimeUnit getDebounceTimeUnit();
}
