package charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.deserialaizers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.ExchangeRatesResponse;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;

public final class ExchangeRatesResponseDeserializer implements JsonDeserializer<ExchangeRatesResponse> {

    @Override
    public ExchangeRatesResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jsonObject = json.getAsJsonObject();
        String base = jsonObject.get("base").getAsString();
        String date = jsonObject.get("date").getAsString();
        Type ratesListType = new TypeToken<List<Rate>>() {
        }.getType();
        List<Rate> ratesList = context.deserialize(jsonObject.get("rates"), ratesListType);
        return new ExchangeRatesResponse(base, date, ratesList);
    }
}
