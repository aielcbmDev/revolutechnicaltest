package charly.baquero.revoluttechnicaltest.screeens.exchangerates;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import charly.baquero.revoluttechnicaltest.R;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.views.RateItemView;

public final class ExchangeRatesAdapter extends RecyclerView.Adapter<ExchangeRatesAdapter.ViewHolder> {
    private static final int HEADER_ITEM = 0;
    private static final int NORMAL_ITEM = 1;

    @NonNull
    private final LiveRatesDataProvider liveRatesDataProvider;
    @NonNull
    private final ExchangeRatesAdapter.OnRateClickListener onRateClickListener;
    @NonNull
    private final RateItemView.OnAmountToConvertListener onAmountToConvertListener;

    ExchangeRatesAdapter(@NonNull LiveRatesDataProvider liveRatesDataProvider, @NonNull ExchangeRatesAdapter.OnRateClickListener onRateClickListener, @NonNull RateItemView.OnAmountToConvertListener onAmountToConvertListener) {
        this.liveRatesDataProvider = liveRatesDataProvider;
        this.onRateClickListener = onRateClickListener;
        this.onAmountToConvertListener = onAmountToConvertListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rate_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        if (viewType == NORMAL_ITEM) {
            viewHolder.itemView.setOnClickListener(v -> {
                int position = viewHolder.getAdapterPosition();
                onRateClickListener.onRateClick(position);
            });
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Rate rate = liveRatesDataProvider.getRateForPosition(position);
        holder.rateItemView.setRate(onAmountToConvertListener, rate, position);
    }

    @Override
    public int getItemCount() {
        return liveRatesDataProvider.getRatesItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? HEADER_ITEM : NORMAL_ITEM;
    }

    static final class ViewHolder extends RecyclerView.ViewHolder {

        @NonNull
        final RateItemView rateItemView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            rateItemView = (RateItemView) itemView;
        }
    }

    public interface OnRateClickListener {

        void onRateClick(int position);
    }
}
