package charly.baquero.revoluttechnicaltest.screeens.exchangerates.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.jakewharton.rxbinding3.widget.RxTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import charly.baquero.revoluttechnicaltest.R;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;
import io.reactivex.Observable;

public final class RateItemView extends RelativeLayout {
    @BindView(R.id.rate_list_item_currency_flag)
    ImageView currencyFlag;
    @BindView(R.id.rate_list_item_currency_name)
    TextView currencyName;
    @BindView(R.id.rate_list_item_exchange_value)
    EditText exchangeValue;

    @NonNull
    private final OnFocusChangeListener onFocusChangeListener = (v, hasFocus) -> {
        if (hasFocus) {
            showKeyboard(v);
        } else {
            hideKeyboard(v);
        }
    };

    public RateItemView(Context context) {
        super(context);
    }

    public RateItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RateItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void setRate(@NonNull OnAmountToConvertListener onAmountToConvertListener, @NonNull Rate rate, int position) {
        currencyName.setText(rate.getName());
        if (position == 0) {
            setOnAmountToConvertListener(onAmountToConvertListener);
            exchangeValue.setText(rate.getAmountToConvert());
            exchangeValue.setSelection(exchangeValue.getText().length());
            exchangeValue.setFocusable(true);
            exchangeValue.setClickable(true);
            exchangeValue.setOnFocusChangeListener(onFocusChangeListener);
        } else {
            exchangeValue.setText(rate.getFormattedConvertedAmount());
            exchangeValue.setFocusable(false);
            exchangeValue.setClickable(false);
        }
    }

    private void showKeyboard(@NonNull View view) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    private void hideKeyboard(@NonNull View view) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void setOnAmountToConvertListener(@NonNull OnAmountToConvertListener onAmountToConvertListener) {
        Observable<String> exchangeValueObservable = RxTextView.textChanges(exchangeValue).map(CharSequence::toString);
        onAmountToConvertListener.onAmountToConvertObservable(exchangeValueObservable);
    }

    public interface OnAmountToConvertListener {

        void onAmountToConvertObservable(@NonNull Observable<String> amountToConvertObservable);
    }
}
