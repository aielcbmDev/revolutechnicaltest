package charly.baquero.revoluttechnicaltest;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public interface MainActivityContract {

    interface View {

        void changeFragment(@NonNull Class<? extends Fragment> fragmentClass);

        void performBackPressed();

        void performFinish();
    }

    interface Presenter {

        void onCreate(@Nullable Bundle savedInstanceState);

        void onBackPressed(int backStackEntryCount);
    }
}
