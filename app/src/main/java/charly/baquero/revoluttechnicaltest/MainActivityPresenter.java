package charly.baquero.revoluttechnicaltest;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import javax.inject.Inject;

import charly.baquero.revoluttechnicaltest.screeens.exchangerates.ExchangeRatesFragment;

public final class MainActivityPresenter implements MainActivityContract.Presenter {

    @NonNull
    private final MainActivityContract.View mainActivityView;

    @Inject
    MainActivityPresenter(@NonNull MainActivityContract.View mainActivityView) {
        this.mainActivityView = mainActivityView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mainActivityView.changeFragment(ExchangeRatesFragment.class);
        }
    }

    @Override
    public void onBackPressed(int backStackEntryCount) {
        if (backStackEntryCount > 1) {
            mainActivityView.performBackPressed();
        } else {
            mainActivityView.performFinish();
        }
    }
}
