package charly.baquero.revoluttechnicaltest;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public final class MainActivity extends DaggerAppCompatActivity implements MainActivityContract.View {

    @Inject
    MainActivityContract.Presenter mainActivityPresenter;
    @Inject
    RevolutApplication revolutApplication;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivityPresenter.onCreate(savedInstanceState);
    }

    @Override
    public void changeFragment(@NonNull Class<? extends Fragment> fragmentClass) {
        if (!isFinishing()) {
            String fragmentClassName = fragmentClass.getName();
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = fragmentManager.findFragmentByTag(fragmentClassName);
            if (fragment == null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
                ClassLoader classLoader = fragmentClass.getClassLoader();
                validateClassLoader(classLoader);
                fragment = fragmentManager.getFragmentFactory().instantiate(classLoader, fragmentClassName);
                fragmentTransaction.replace(R.id.main_activity_fragment_container, fragment, fragmentClassName);
                fragmentTransaction.addToBackStack(fragmentClassName);
                fragmentTransaction.commit();
            } else {
                fragmentManager.popBackStackImmediate();
            }
        }
    }

    @Override
    public void performBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void performFinish() {
        finish();
    }

    private void validateClassLoader(@Nullable ClassLoader classLoader) {
        // This should NEVER happen
        if (classLoader == null) {
            throw new RuntimeException("classLoader can NOT be NULL");
        }
    }

    @Override
    public void onBackPressed() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        mainActivityPresenter.onBackPressed(backStackEntryCount);
    }
}
