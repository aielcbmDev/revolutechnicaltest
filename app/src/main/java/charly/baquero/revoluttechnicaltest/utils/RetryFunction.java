package charly.baquero.revoluttechnicaltest.utils;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public final class RetryFunction implements Function<Observable<Throwable>, ObservableSource<?>> {

    private final int numberOfRetries;
    private final long timeBetweenRetries;
    @NonNull
    private final TimeUnit timeUnit;

    public RetryFunction(int numberOfRetries, long timeBetweenRetries, @NonNull TimeUnit timeUnit) {
        this.numberOfRetries = numberOfRetries;
        this.timeBetweenRetries = timeBetweenRetries;
        this.timeUnit = timeUnit;
    }

    @NonNull
    @Override
    public ObservableSource<?> apply(@NonNull Observable<Throwable> throwableObservable) {
        AtomicInteger counter = new AtomicInteger();
        return throwableObservable
                .takeWhile(throwable -> counter.getAndIncrement() < numberOfRetries)
                .flatMap((Function<Throwable, ObservableSource<?>>) throwable -> Observable.timer(timeBetweenRetries, timeUnit));
    }
}
