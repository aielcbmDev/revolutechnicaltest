package charly.baquero.revoluttechnicaltest.utils;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import charly.baquero.revoluttechnicaltest.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public final class OkHttpClientProvider {

    @NonNull
    private static final HttpLoggingInterceptor.Level LOGGING_LEVEL = BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE;

    @NonNull
    private final OkHttpClient sharedOkHttpClient;

    public OkHttpClientProvider(long timeout, @NonNull TimeUnit timeUnit) {
        sharedOkHttpClient = new OkHttpClient.Builder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .connectTimeout(timeout, timeUnit)
                .build();
    }

    @NonNull
    public OkHttpClient provideSharedOkHttpClient() {
        return sharedOkHttpClient;
    }

    @NonNull
    private static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.level(LOGGING_LEVEL);
        return httpLoggingInterceptor;
    }

    @NonNull
    public static OkHttpClient generateSpecificOkHttpClient(@NonNull OkHttpClient sharedOkHttpClient) {
        return sharedOkHttpClient.newBuilder().build();
    }
}
