package charly.baquero.revoluttechnicaltest;

import androidx.annotation.NonNull;

import charly.baquero.revoluttechnicaltest.di.scopes.PerActivity;
import dagger.Module;
import dagger.Provides;

@Module
public final class MainActivityModule {

    @NonNull
    @Provides
    @PerActivity
    MainActivityContract.View provideMainActivityView(@NonNull MainActivity mainActivity) {
        return mainActivity;
    }

    @NonNull
    @Provides
    @PerActivity
    MainActivityContract.Presenter provideMainActivityPresenter(@NonNull MainActivityContract.View mainActivityView) {
        return new MainActivityPresenter(mainActivityView);
    }
}
