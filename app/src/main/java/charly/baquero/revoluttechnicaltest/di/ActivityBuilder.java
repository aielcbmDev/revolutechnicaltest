package charly.baquero.revoluttechnicaltest.di;

import charly.baquero.revoluttechnicaltest.MainActivity;
import charly.baquero.revoluttechnicaltest.MainActivityModule;
import charly.baquero.revoluttechnicaltest.di.scopes.PerActivity;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.ExchangeRatesDependenciesProvider;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {MainActivityModule.class, ExchangeRatesDependenciesProvider.class})
    @PerActivity
    public abstract MainActivity bindMainActivity();
}
