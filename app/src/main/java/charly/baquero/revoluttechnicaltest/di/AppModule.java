package charly.baquero.revoluttechnicaltest.di;

import android.app.Application;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import charly.baquero.revoluttechnicaltest.RevolutApplication;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.CurrencyExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.calculators.ExchangeCalculator;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter.CurrencyFormatter;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.currencyformatter.CurrencyFormatterProvider;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.deserialaizers.ExchangeRatesResponseDeserializer;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.deserialaizers.RatesDeserializer;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.ExchangeRatesResponse;
import charly.baquero.revoluttechnicaltest.screeens.exchangerates.network.response.Rate;
import charly.baquero.revoluttechnicaltest.utils.OkHttpClientProvider;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

@Module
final class AppModule {

    @NonNull
    @Provides
    @Singleton
    ExchangeCalculator provideExchangeCalculator() {
        String defaultCurrencyNameToConvert = "EUR";
        BigDecimal defaultAmountToConvert = BigDecimal.ONE;
        return new CurrencyExchangeCalculator(defaultCurrencyNameToConvert, defaultAmountToConvert);
    }

    @NonNull
    @Provides
    @Singleton
    CurrencyFormatter provideCurrencyFormatter() {
        int numberOfDecimals = 4;
        // https://stackoverflow.com/a/8209046
        RoundingMode roundingMode = RoundingMode.HALF_EVEN;
        boolean stripTrailingZeros = true;
        return new CurrencyFormatterProvider(numberOfDecimals, roundingMode, stripTrailingZeros);
    }

    @NonNull
    @Provides
    @Singleton
    RevolutApplication provideRevolutApplication(@NonNull Application application) {
        return (RevolutApplication) application;
    }

    @NonNull
    @Provides
    @Singleton
    OkHttpClient provideSharedOkHttpClient() {
        OkHttpClientProvider okHttpClientProvider = new OkHttpClientProvider(500L, TimeUnit.MILLISECONDS);
        return okHttpClientProvider.provideSharedOkHttpClient();
    }

    @NonNull
    @Provides
    @Singleton
    Gson provideGson(@NonNull ExchangeCalculator exchangeCalculator, @NonNull CurrencyFormatter currencyFormatter) {
        return new GsonBuilder()
                .registerTypeAdapter(ExchangeRatesResponse.class, new ExchangeRatesResponseDeserializer())
                .registerTypeAdapter(new TypeToken<List<Rate>>() {
                }.getType(), new RatesDeserializer(exchangeCalculator, currencyFormatter))
                .create();
    }
}
