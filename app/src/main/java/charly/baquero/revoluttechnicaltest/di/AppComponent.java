package charly.baquero.revoluttechnicaltest.di;

import android.app.Application;

import javax.inject.Singleton;

import charly.baquero.revoluttechnicaltest.RevolutApplication;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void inject(RevolutApplication revolutApplication);
}
