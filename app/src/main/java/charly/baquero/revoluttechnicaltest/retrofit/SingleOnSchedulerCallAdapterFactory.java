package charly.baquero.revoluttechnicaltest.retrofit;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import io.reactivex.Scheduler;
import io.reactivex.Single;

public final class SingleOnSchedulerCallAdapterFactory extends RxObserveOnSchedulerCallAdapterFactory<Single<?>> {

    @NonNull
    private static final Class<Single> CLAZZ = Single.class;

    private SingleOnSchedulerCallAdapterFactory(@NonNull Scheduler scheduler) {
        super(scheduler);
    }

    @NotNull
    @Override
    protected Single<?> observeOnScheduler(@NonNull Single<?> o, @NonNull Scheduler mainThreadScheduler) {
        return o.observeOn(mainThreadScheduler);
    }

    @Override
    protected boolean isWrongRawClass(@NotNull Class<?> returnClazz) {
        return returnClazz != CLAZZ;
    }

    @NonNull
    public static SingleOnSchedulerCallAdapterFactory create(@NonNull Scheduler scheduler) {
        return new SingleOnSchedulerCallAdapterFactory(scheduler);
    }
}
