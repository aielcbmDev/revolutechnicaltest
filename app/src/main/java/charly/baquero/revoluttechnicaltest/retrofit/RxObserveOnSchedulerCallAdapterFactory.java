package charly.baquero.revoluttechnicaltest.retrofit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import io.reactivex.Scheduler;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;

/**
 * "Making Retrofit Work For You" by Jake Wharton
 * <p>
 * Please, watch minute 46:47
 * https://www.youtube.com/watch?v=t34AQlblSeE
 */
abstract class RxObserveOnSchedulerCallAdapterFactory<T> extends CallAdapter.Factory {

    @NonNull
    private final Scheduler scheduler;

    RxObserveOnSchedulerCallAdapterFactory(@NonNull Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public CallAdapter<?, ?> get(@NotNull Type returnType, @NotNull Annotation[] annotations, @NotNull Retrofit retrofit) {
        Class<?> returnClazz = getRawType(returnType);
        if (isWrongRawClass(returnClazz)) {
            return null;
        }

        CallAdapter<Object, ?> delegate = (CallAdapter<Object, ?>) retrofit.nextCallAdapter(this, returnType, annotations);

        return new CallAdapter<Object, T>() {
            @NotNull
            @Override
            public Type responseType() {
                return delegate.responseType();
            }

            @NotNull
            @Override
            public T adapt(@NotNull Call<Object> call) {
                T o = (T) delegate.adapt(call);
                return observeOnScheduler(o, scheduler);
            }
        };
    }

    @NonNull
    protected abstract T observeOnScheduler(@NonNull T o, @NonNull Scheduler scheduler);

    protected abstract boolean isWrongRawClass(@NotNull Class<?> returnClazz);
}